class CreateBillings < ActiveRecord::Migration
  def change
    create_table :billings do |t|
      t.string :address
      t.string :city
      t.string :country
      t.string :postal_code
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
