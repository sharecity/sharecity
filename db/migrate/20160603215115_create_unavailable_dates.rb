class CreateUnavailableDates < ActiveRecord::Migration
  def change
    create_table :unavailable_dates do |t|
      t.references :item, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
