class AddDateNoteToRequestedItem < ActiveRecord::Migration
  def change
    add_column :requested_items, :date_note, :string
  end
end
