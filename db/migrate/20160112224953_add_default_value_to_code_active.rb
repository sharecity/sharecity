class AddDefaultValueToCodeActive < ActiveRecord::Migration
  def up
    change_column :users, :code_active, :string, :default => "not active"
  end

  def down
    change_column :users, :code_active, :string, :default => r
  end
end
