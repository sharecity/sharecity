class AddReservationToMessages < ActiveRecord::Migration
  def change
    add_reference :messages, :reservation, index: true, foreign_key: true
  end
end
