class AddSharingToItems < ActiveRecord::Migration
  def change
    add_column :items, :sharing, :string
  end
end
