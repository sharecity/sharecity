class CreatePaypalAccounts < ActiveRecord::Migration
  def change
    create_table :paypal_accounts do |t|
      t.string :paypal_email
      t.string :currency

      t.timestamps null: false
    end
  end
end
