class AddDateToUnavailableDates < ActiveRecord::Migration
  def change
    add_column :unavailable_dates, :date, :date
  end
end
