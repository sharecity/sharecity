class CreateClaims < ActiveRecord::Migration
  def change
    create_table :claims do |t|
      t.string :confirmation_number
      t.string :note
      t.string :status, default: "new"
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
