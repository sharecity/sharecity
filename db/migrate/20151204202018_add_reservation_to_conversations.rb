class AddReservationToConversations < ActiveRecord::Migration
  def change
    add_reference :conversations, :reservation, index: true, foreign_key: true
  end
end
