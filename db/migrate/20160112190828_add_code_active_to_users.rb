class AddCodeActiveToUsers < ActiveRecord::Migration
  def change
    add_column :users, :code_active, :string
  end
end
