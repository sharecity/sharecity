class AddCheckedoutRequesterToReservations < ActiveRecord::Migration
  def change
    add_column :reservations, :checkedout_requester, :string, :default => "no"
  end
end
