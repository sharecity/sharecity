class AddItemOwnerIdToReservations < ActiveRecord::Migration
  def change
    add_column :reservations, :item_owner_id, :integer
  end
end
