class ChangeColumnType < ActiveRecord::Migration
  def change
    change_column :transactions, :payout, 'decimal USING CAST(payout AS decimal)'
  end
end
