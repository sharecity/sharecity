class AddCheckedoutOwnerToReservations < ActiveRecord::Migration
  def change
    add_column :reservations, :checkedout_owner, :string, :default => "no"
  end
end
