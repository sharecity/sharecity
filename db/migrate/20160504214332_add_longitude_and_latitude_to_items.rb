class AddLongitudeAndLatitudeToItems < ActiveRecord::Migration
  def change
    add_column :items, :longitude, :float
    add_column :items, :latitude, :float
  end
end
