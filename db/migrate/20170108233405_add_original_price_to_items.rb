class AddOriginalPriceToItems < ActiveRecord::Migration
  def change
    add_column :items, :original_price, :string
  end
end
