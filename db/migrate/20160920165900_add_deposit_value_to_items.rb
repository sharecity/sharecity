class AddDepositValueToItems < ActiveRecord::Migration
  def change
    add_column :items, :damage_deposit, :decimal, default: 0
  end
end
