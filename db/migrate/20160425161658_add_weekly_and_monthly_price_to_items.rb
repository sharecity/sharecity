class AddWeeklyAndMonthlyPriceToItems < ActiveRecord::Migration
  def change
    add_column :items, :weekly_price, :decimal
    add_column :items, :monthly_price, :decimal
  end
end
