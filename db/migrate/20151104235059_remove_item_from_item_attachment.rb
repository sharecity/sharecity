class RemoveItemFromItemAttachment < ActiveRecord::Migration
  def change
    remove_column :item_attachments, :item_id
  end
end
