class AddItemToItemAttachments < ActiveRecord::Migration
  def change
    add_reference :item_attachments, :item, index: true, foreign_key: true
  end
end
