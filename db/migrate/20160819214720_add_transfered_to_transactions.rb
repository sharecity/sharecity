class AddTransferedToTransactions < ActiveRecord::Migration
  def change
    add_column :transactions, :transfered, :string, :default => "no"
  end
end
