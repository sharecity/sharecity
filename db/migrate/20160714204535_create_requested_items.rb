class CreateRequestedItems < ActiveRecord::Migration
  def change
    create_table :requested_items do |t|
      t.string :name
      t.string :location
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
