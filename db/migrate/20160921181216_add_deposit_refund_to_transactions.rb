class AddDepositRefundToTransactions < ActiveRecord::Migration
  def change
    add_column :transactions, :deposit_refund, :string
  end
end
