class AddFieldsToRequestedItems < ActiveRecord::Migration
  def change
    remove_column :requested_items, :date_note
    add_column :requested_items, :from_date, :datetime
    add_column :requested_items, :to_date, :datetime
    add_column :requested_items, :description, :text
  end
end
