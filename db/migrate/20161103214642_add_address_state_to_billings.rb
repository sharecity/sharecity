class AddAddressStateToBillings < ActiveRecord::Migration
  def change
    add_column :billings, :address_state, :string
  end
end
