class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.string :amount
      t.string :date
      t.string :confirmation_id
      t.string :fees
      t.string :payout
      t.references :item, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.references :reservation, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
