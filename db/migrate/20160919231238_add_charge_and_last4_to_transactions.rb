class AddChargeAndLast4ToTransactions < ActiveRecord::Migration
  def change
    add_column :transactions, :charge, :string
    add_column :transactions, :last4, :string
  end
end
