class AddDamageDepositToTransactions < ActiveRecord::Migration
  def change
    add_column :transactions, :damage_deposit, :decimal, default: 0
    add_column :transactions, :deposit_charge, :string
  end
end
