class AddPriceStatusToItems < ActiveRecord::Migration
  def change
    add_column :items, :price_status, :string
  end
end
