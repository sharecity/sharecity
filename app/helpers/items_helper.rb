module ItemsHelper
  def pricetag(item, item_type='')
    case item_type
    when 'weekly'
      price = item.weekly_price
    when 'monthly'
      price = item.monthly_price
    when 'damage'
      price = item.damage_deposit
    else
      price = item.price
    end
    item.free? ? 'FREE' : number_to_currency(price)
  end

  # def tag_links(tags)
  #   tags.split(",").map{|tag| link_to tag.strip, tag_path(tag.strip) }.join(", ")
  # end

end
