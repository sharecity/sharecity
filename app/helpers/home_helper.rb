module HomeHelper
  def tags(item)
    if item.tags
      item_tags = item.tags.pluck(:name).map do |tag| 
        link_to "##{tag}", tagged_items_path(tagname: tag)
      end
      item_tags.join(', ').html_safe
    end
  end

  def tag_cloud(item)
    if item.tags
      item_tags = item.tags.pluck(:name).map do |tag| 
        link_to "#{tag}", tagged_items_path(tagname: tag)
      end
      item_tags.join(' ').html_safe
    end
  end

  def format_date(date)
    date.strftime('%B %d, %Y')
  end
end
