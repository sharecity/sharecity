module ConversationsHelper
  def avatar(user)
    case user.provider
    when 'facebook'
      "//graph.facebook.com/#{user.uid}/picture?type=square"
    when 'google_oauth2'
      user.image
    else
      user.avatar.url
    end
  end

  def recipient_name(conversation)
    conversation.recipient.full_name || conversation.recipient.name
  end

  def sender_name(conversation)
    conversation.sender.full_name || conversation.sender.name
  end
end
