module ReservationsHelper
  def pending_request?(item)
    item.reservations.where('user_id = ? AND status = ?', current_user, 'requested').any?
  end
  def pending_request_live?

  end
end
