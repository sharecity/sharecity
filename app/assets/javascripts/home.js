jQuery(function($) {
  var current_id = $('#current-user').attr('data-user_id');
  var item_owner = $('.itemUsrName').attr('data-username');
  var item_owner_id = $('.itemUsrName').attr('data-user_id');

  function reqDate(date) {
    var months = ['January', 'February', 'March',
                  'April', 'May', 'June', 'July',
                  'August', 'September', 'October',
                  'November', 'December']
    var d = date.split('-');
    var year = d[0];
    var month = months[ d[1] - 1 ];
    var day = d[2].split('T')[0];
    return month + ' ' + day + ', ' + year
  }

  function feedItem(data, feedId) {
    var feed = $(feedId);
    var itemId;

    switch(feedId) {
      case '#request-feed':
        itemId = 'feed-item-';
        break;
      case '#your-requests':
        itemId = 'your-request-';
        break;
      default:
        itemId = 'request-item-';
        break;
    }

    var item = $('<div>').addClass('feed-item').attr('id', itemId + data.res_id).prependTo(feed);

    if (feedId === '#your-requests') {
      item.append('You ');
    }
    else {
      $('<a>').attr('href', '/users/' + data.requester_id)
        .html('<strong>' + data.requester + '</strong>')
        .appendTo(item);
    }
    item.append(' requested ');
    $('<a>').attr('href', '/items/' + data.item_id).html('<strong>' + data.item + '</strong>').appendTo(item);

    if (feedId === '#request-feed') {
      item.append(' from ');
      $('<a>').attr('href', '/users/' + data.owner_id).text(data.owner).appendTo(item);
      item.append(' for ' + [data.start, data.end].join(' to ') );

      var accept = $('<div>').addClass('accept-res').appendTo(item);
      $('<a>').attr('href', '/conversations').text('Contact').appendTo(accept);
    }
    else if (feedId == '#your-requests') {
      item.append(' from ');
      $('<a>').attr('href', '/users/' + data.owner_id).text(data.owner).appendTo(item);
      $('<a>').attr('href', data.cancel_path)
              .text('Cancel')
              .attr('data-remote', true)
              .css('margin', 'auto 2px')
              .attr('data-status', 'cancelled-request')
              .addClass('btn btn-danger cancelBtn reqBtn')
              .appendTo(item);
    }
    else {
      // item.append(' for ' + [data.start, data.end].join(' to ') );

      buildBtnDetails(data.item_id, data.res_id, 'approved-owner').appendTo(item);
      // buildBtn(data.item_id, data.res_id, 'rejected-owner').appendTo(item);
    }
  }

  function buildBtn(item_id, res_id, reqStatus) {
    var btnLink = $('<a>').attr('href', '/items/'+item_id+'/reservations/'+res_id+'?status=' + reqStatus);

    if (reqStatus == 'approved-owner') {
      btnLink.addClass('btn btn-success reqBtn')
      .css('margin', 'auto 2px')
      .text('Accept');
    }
    else {
      btnLink.addClass('btn btn-danger reqBtn')
      .css('margin', 'auto 2px')
      .text('Reject');
    }
    return btnLink;
  }

  function buildBtnDetails(item_id, res_id, reqStatus) {
    // var btnLink = $('<a>').attr('href', '#' + '?id=' + res_id);
    var btnLink = $('<a>').attr({ href:'#', id: res_id});

    if (reqStatus == 'approved-owner') {
      btnLink.addClass('btn btn-success reqBtn')
      .css('margin', 'auto 2px')
      .text('Click for details');
    }
    return btnLink;
  }

  function acceptRes(data, type) {
    var reservations = $('#accepted-reservations');
    var item = $('<div>').addClass('feed-item').attr('id', 'accepted-request-' + data.res_id).appendTo(reservations);
    var cancelStatus = 'cancelled';

    $('<strong>').text(data.requester).appendTo(item);
    if ( type === 'owner' ) {
      // IF YOU'RE THE ONE APPROVING THE REQUEST
      item.append('</strong> you accepted a requet for <strong><a href="' +data.item_path+'">' + data.item + '</a></strong> for ' + [data.start, data.end].join(' to ') )
      cancelStatus = 'cancelled-owner';
    }
    else {
      // IF YOU'RE THE ITEM REQUESTER
      item.append(' confirm your request for <strong><a href="'+ data.item_path + '">' + data.item + '</a></strong> for ' + [data.start, data.end].join(' to ') );
      if (data.price > 0) {
        $('<a>').text('Pay')
                .addClass('btn btn-success reqBtn btn_pay')
                .appendTo(item);
        $('<div>').addClass('it hidden').attr('id', data.item_id).appendTo(item);
        $('<div>').addClass('re hidden').attr('id', data.res_id).appendTo(item);
      }
    }

    // BOTH REQUESTER & OWNER HAS A CANCELLATION OPTION
    $('<a>').attr('href', data.cancel_path)
            .text('Cancel')
            .css('margin', 'auto 2px')
            .attr('data-status', cancelStatus)
            .attr('date-remote', true)
            .addClass('btn btn-danger cancelBtn reqBtn')
            .attr('id', 'btn_cancel')
            .appendTo(item);
  }

  // CHANNEL NAMES
  var itemFeed = '/item-feed';
  //var following = '/item-feed/following-';
  //var follower = '/item-feed/follower-';
  var reqReceived = '/received-item-requests/user-';
  var yourReqs = '/your-item-requests/user-';
  var reqStatusUpdate = '/accepted-reservations/user-';

  function itemTags(tags) {
    //tags = tags.replace(/[\"\[\]]/g, '').split(', ');
    var allTags = [];
    tags.forEach(function(t) {
      allTags.push('<a href="/tags/'+ t +'">#' + t + '</a>');
    });
    return allTags.join(',');
  }

  function buildItem(data) {
    var feed = $('#item-post-feed');
    var itemId = 'posted-item-' + data.item_id;
    var item = $('<div>').addClass('feed-item').attr('id', itemId).prependTo(feed);
    var row = $('<div>').addClass('row').appendTo(item);
    var rowInfo = $('<div>').addClass('row rowInfo').appendTo(item);
    var col3 = $('<div>').addClass('col-xs-3 col-sm-2').appendTo(row);
    var col9 = $('<div>').addClass('col-xs-9 col-sm-10').appendTo(row);
    var col9Row = $('<div>').addClass('row').appendTo(col9);
    var itemUrl = '/items/'+data.item_id

    $('<img>').attr('src', data.img).appendTo(col3);
    $('<div>').addClass('col-xs-8')
      .html('<a href="'+itemUrl+ '" class="item-name">' + data.item + '</a>')
      .appendTo(col9Row);
    $('<div>').addClass('col-xs-4 bookDiv')
      .html('<span class="book-item"><a href="' + itemUrl +'">Request</a></span>')
      .appendTo(col9Row);
    col9.append(data.desc);
    $('<div>').html( itemTags(data.tags) ).appendTo(col9);
    $('<div>').html('<strong>Owner:</strong> <a href="/users/' + data.owner_id + '">' + data.owner + '</a>').appendTo(col9);

    // ROW INFO
    $('<div>').addClass('col-xs-4')
      .html('<strong>Price:</strong> ' + data.price)
      .appendTo(rowInfo);
    $('<div>').addClass('col-xs-7')
      .html('<strong>Postal code:</strong> ' + data.postal)
      .appendTo(rowInfo);
  }

  // data is coming from channel subscribe
  function buildItem2(data) {
    var feed = $('#item-post-feed-live');
    var itemId = 'posted-item-live-' + data.item_id;
    var item = $('<div>').addClass('feed-item2').attr('id', itemId).prependTo(feed);
    var row = $('<div>').addClass('row').appendTo(item);
    var rowInfo = $('<div>').addClass('row rowDivder').appendTo(item);
    var col2 = $('<div>').addClass('col-xs-2').appendTo(row);
    var col10 = $('<div>').addClass('col-xs-10').appendTo(row);
    var col9Row = $('<div>').addClass('row').appendTo(col10);

    $('<img>').attr('src', data.imgOwner).appendTo(col2);
    $('<div>').addClass('col-lg-7')
      .html( data.owner + data.owner2 + 'Shared a new Item' + '<span class="item-name">' + data.item + '</span>')
      .appendTo(col9Row);
    $('<div>').addClass('col-lg-5')
      .html('<i class="fa fa-clock-o" aria-hidden="true"></i>' + '1d')
      .appendTo(col9Row);
    col9Row.append('<div>' + data.desc + '</div>');
    $('<div>').html( itemTags(data.tags) ).appendTo(col9Row);
  }

  MessageBus.subscribe(itemFeed, function(data) {
    buildItem(data);
    buildItem2(data);
  });

  MessageBus.subscribe(reqReceived + current_id, function(data) {
    feedItem(data, '#received-requests');
  });

  MessageBus.subscribe(yourReqs + current_id, function(data) {
    feedItem(data, '#your-requests');
  });

  MessageBus.subscribe(reqStatusUpdate + current_id, function(data) {
    var yourReq =  $('#your-request-'+ data.res_id);
    var request = $('#request-item-'+data.res_id);
    if (data.status === 'accepted') {
      acceptRes(data, data.role);
      yourReq.hide();
      request.hide();
    }
    else {
      var accepted = $('#accepted-request-'+data.res_id);
      [accepted, yourReq, request].forEach(function(r) {
        r.empty().text('The request for ' + data.item + ' has been ' + data.status);
      });
    }
  });

  //MessageBus.subscribe(following + current_id + '/*', function(data) {
  //  buildItem(data);
  //});

  //MessageBus.subscribe(follower + current_id + '/*', function(data) {
  //  buildItem(data);
  //});


  //function getFollowers(user) {
  //  var followers = [];
  //  user.followers.forEach(function(f) {
  //    followers.push(f.id);
  //  });
  //  return followers;
  //}

  $('#new_reservation').on('ajax:success', function(e, res) {
    $(this).empty();
    $(this).text('Your request has been sent');
  });

  // ACCEPT/REJECT REQUESTS BY THE OWNER MODAL
  $('#received-requests').on('click', '.reqBtn', function(e) {
    e.preventDefault();
    var reservation_id = $(this)[0].id;
    $('.modalAccept-' + reservation_id ).modal('show');
  });

  // ACCEPT/REJECT REQUESTS BY THE OWNER
  $('.owner-accept-btn, .owner-reject-btn').on('click', function(e) {
     e.preventDefault();
     // var btn = $(this);
     // var btnText = btn.text();
     // btn.text('Saving...');
     var reservation_id = $(this)[0].id;
     var $modalBody = $('.modalAccept-' + reservation_id );
     $.ajax({
       url: $(this).attr('href'),
       type: 'PUT',
       data: {status: $(this).attr('data-status')},
    }).done(function() {
        $modalBody.modal('hide');
       });
   });

  // CANCEL REQUESTS
  $('#accepted-reservations, #your-requests').on('click', '.cancelBtn', function(e) {
    e.preventDefault();
    $(this).prop('disabled', true).text('Saving...');
    $.ajax($(this).attr('href'),
      { dataType: 'json',
        method: 'PATCH',
        data: { status: $(this).attr('data-status') }
      });
  });

  // $('#new_billing_modal').hide();
  // $('#myModalBilling').modal('hide')
  // $('#myModalBillingMobile').modal('hide');

  // usersIdsBilling = [];
  //
  // $.ajax('/billings/',
  //   { dataType: 'json',
  //     method: 'GET',
  //     success: function(server_response){
  //       console.log(server_response);
  //       $.each(server_response.billings, function(arrID, server_response) {
  //         d = server_response.user.id
  //         usersIdsBilling.push(d);
  // 				console.log(server_response.user.id);
  // 			});
  //     }
  //   }).done(function() {
  //     var usr_id = parseInt($('.currUsId')[0].id)
  //     var checkArray = $.inArray(usr_id, usersIdsBilling);
  //     if (checkArray === -1) {
  //       var usrD = $('.usrIdStatus')[0]
  //       usrD.id = 0;
  //       console.log("This user has no billing info");
  //     } else {
  //       var usrD = $('.usrIdStatus')[0]
  //       usrD.id = 1;
  //       console.log("this user has billing info");
  //     }
  //   });

    // For Desktop
    // $('#btn_book').on('click', function(e){
    //     var userStatus = $('.usrIdStatus')[0].id
    //     // 0 means no billings info - 1 means has billings
    //     if (userStatus == 0) {
    //       // debugger;
    //       e.preventDefault();
    //       $('#myModalBilling').modal('show');
    //     } else {
    //       var startDate = $('#reservation_start_date').val();
    //       var endDate   = $('#reservation_end_date').val();
    //       var item_id   = $('.itemId')[0].id
    //       var sharingStat   = $('.sharingStatus')[0].id
    //       $.ajax({
    //         type: "POST",
    //         data: {strDate: startDate, enDate: endDate, itemId: item_id, priceItem: 0, totalItem: 0, statusItem: "requested"},
    //         url: "/items/" + item_id + "/reservations/",
    //         success: function(res){
    //           console.log(res.status);
    //           // window.location = "http://www.sharecity.net";
    //           // $('#new_reservation').on('ajax:success', function(e, res) {
    //             // alert("dsdfdfdfd");
    //             $('#new_reservation').empty();
    //             $('#preview').empty();
    //             $('#btn_book').hide();
    //             $('#new_reservation').text('Your request has been sent');
    //             var followers = getFollowers( res.item.user );
    //             current_id = parseInt( current_id );
    //             var feedInfo = { requester: res.user.first_name, requester_id: res.user_id,
    //                 item: res.item.name, item_id: res.item_id,
    //                 start: reqDate(res.start_date), end: reqDate(res.end_date),
    //                 owner: item_owner, owner_id: res.item.user_id }
    //             if (res.item.sharing === '0') {
    //               window.client.publish(reqFeed, feedInfo);
    //             }
    //             else {
    //               followers.forEach(function(f) {
    //                 window.client.publish(reqFeed + '/' + f, feedInfo);
    //               });
    //             }
    //             window.client.publish(reqReceived + res.item.user_id,
    //               { requester: res.user.first_name, requester_id: res.user_id,
    //                 item: res.item.name,
    //                 item_id: res.item_id, res_id: res.id,
    //                 start: reqDate(res.start_date), end: reqDate(res.end_date) });
    //             window.client.publish(yourReqs + current_id, feedInfo);
    //           // });
    //         }
    //       });
    //     }
    //   });


      // For Mobile
      $('#btn_bookMobile').on('click', function(e){
          var userStatus = $('.usrIdStatus')[0].id
          // 0 means no billings info - 1 means has billings
          if (userStatus === "0") {
            e.preventDefault();
            console.log("modal mobile");
            $('#myModalBillingMobile').modal('show');
          } else {
            var startDate = $('#startDate').val();
            var endDate   = $('#endDate').val();
            var item_id   = $('.itemId')[0].id
            var sharingStat   = $('.sharingStatus')[0].id

            $.ajax({
              type: "POST",
              data: {strDate: startDate, enDate: endDate, itemId: item_id, priceItem: 0, totalItem: 0, statusItem: "requested"},
              url: "/items/" + item_id + "/reservations/",
              success: function(res){
                console.log(res.status);
                // window.location = "http://www.sharecity.net";
                // $('#new_reservation').on('ajax:success', function(e, res) {
                  // alert("dsdfdfdfd");
                  $('#previewMobile').empty();
                  $('#btn_bookMobile').hide();
                  $('#new_reservation_mobile').empty();
                  $('#new_reservation_mobile').text('Your request has been sent');
                  //var followers = getFollowers( res.item.user );
                  //current_id = parseInt( current_id );
              }
            });
          }
        });

        // Modal Billing request
        // $('#submitBill').on('click', function(e) {
        //   e.preventDefault();
        //   var add = $('#billing_address').val();
        //   var cit = $('#billing_city').val();
        //   var cou = $('#billing_country').val();
        //   var pos = $('#billing_postal_code').val();
        //   var usr_id = parseInt($('.currUsId')[0].id)
        //   var strToke = $('input[name=stripeToken]').val();
        //   $.ajax({
        //     type: "POST",
        //     data: {addressJ: add, cityJ: cit, countryJ: cou, postalJ: pos, usrId: usr_id, sTokenJ: strToke},
        //     url: "/billings/",
        //     success: function(res){
        //       console.log(res.status);
        //       $('#myModalBillingMobile').modal('hide');
        //     }
        //   });
        // });

  // $('#btn_book').on('click', function(e){
  //     e.preventDefault();
  //     var item_id = $('.itemId')[0].id
  //     console.log(item_id);
  //     $.ajax({
  //       type: "GET",
  //       url: "/items/"+item_id,
  //       success: function(server_response){
  //         if (server_response['message'] == 'NeedsBilling') {
  //           alert("NeedsBilling");
  //         } else if (server_response['message'] == 'BillingExists') {
  //           alert("BillingExists");
  //         }
  //       }
  //     });
  //   });
  //});

  // home page buttons (mobile)
  $('#btn1').on('click', function () {
    if ($('#requestedFeed').hasClass("desktop")) {
      $('.feedMobile').fadeOut(2000, function (){
        $('#requestedFeed').removeClass("desktop");
        $('#requestedFeed').addClass("mobile");
        $('.hideForFe').removeClass("mobile");
        $('.hideForFe').addClass("desktop");
      });
    } else if ($('#requestedFeed').hasClass("mobile")) {
      $('.feedMobile').fadeIn(2000, function (){
        $('#requestedFeed').removeClass("mobile");
        $('#requestedFeed').addClass("desktop");
        $('.hideForFe').removeClass("mobile");
        $('.hideForFe').addClass("desktop");
      });
    }
  });

  $('#btn2').on('click', function () {
    if ($('#requestedPending').hasClass("desktop")) {
      $('.feedMobile').fadeOut(2000, function (){
        $('#requestedPending').removeClass("desktop");
        $('#requestedPending').addClass("mobile");
        $('.hideForPe').removeClass("mobile");
        $('.hideForPe').addClass("desktop");
      });
    } else if ($('#requestedPending').hasClass("mobile")) {
      $('.feedMobile').fadeIn(2000, function (){
        $('#requestedPending').removeClass("mobile");
        $('#requestedPending').addClass("desktop");
        $('.hideForPe').removeClass("mobile");
        $('.hideForPe').addClass("desktop");
      });
    }
  });

  $('#btn3').on('click', function () {
    if ($('#requestedReceived').hasClass("desktop")) {
      $('.feedMobile').fadeOut(2000, function (){
        $('#requestedReceived').removeClass("desktop");
        $('#requestedReceived').addClass("mobile");
        $('.hideForRe').removeClass("mobile");
        $('.hideForRe').addClass("desktop");
      });
    } else if ($('#requestedReceived').hasClass("mobile")) {
      $('.feedMobile').fadeIn(2000, function (){
        $('#requestedReceived').removeClass("mobile");
        $('#requestedReceived').addClass("desktop");
        $('.hideForRe').removeClass("mobile");
        $('.hideForRe').addClass("desktop");
      });
    }
  });

});
