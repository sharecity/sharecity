$(document).ready(function() {

  // Check in - requester
  $('.checkIn-requester').on('click', function(e) {
    e.preventDefault();
    e.stopPropagation();
    var reservation_id = $('.checkIn-requester')[0].id
    $.ajax({
      type: "PUT",
      data: {reservationId: reservation_id, checkingInRequester: "yes"},
      url: "/reservations/" +reservation_id+ "/updateCheck/",
      success: function(xhr){
        console.log(xhr.status);
        location.reload();
        // $('.checkIn-requester').css({"background-color": "#d9534f", "border": "none"});
      }
    });
  });

  // check in - owner
  $('.checkIn-owner').on('click', function(e) {
    e.preventDefault();
    e.stopPropagation();
    var reservation_id = $('.checkIn-owner')[0].id
    $.ajax({
      type: "PUT",
      data: {reservationId: reservation_id, checkingInOwner: "yes"},
      url: "/reservations/" +reservation_id+ "/updateCheck/",
      success: function(xhr){
        console.log(xhr.status);
        $('.checkIn-owner').css({"background-color": "#dff0d8", "border": "none"});
        location.reload();
      }
    });
  });

  //  check out - owner
  $('.checkOut-owner').on('click', function(e) {
    e.preventDefault();
    e.stopPropagation();
    var reservation_id = $('.checkOut-owner')[0].id
    $.ajax({
      type: "PUT",
      data: {reservationId: reservation_id, checkingOutOwner: "yes"},
      url: "/reservations/" +reservation_id+ "/updateCheck/",
      success: function(xhr){
        console.log(xhr.status);
        $('.checkOut-owner').css({"background-color": "#dff0d8", "border": "none"});
        $('.checkOut-owner').attr('disabled', 'disabled');
        location.reload();
      }
    });
  });

  //  check out - requester
  $('.checkOut-requester').on('click', function(e) {
    e.preventDefault();
    e.stopPropagation();
    var reservation_id = $('.checkOut-requester')[0].id
    $.ajax({
      type: "PUT",
      data: {reservationId: reservation_id, checkingOutRequester: "yes"},
      url: "/reservations/" +reservation_id+ "/updateCheck/",
      success: function(xhr){
        console.log(xhr.status);
        location.reload();
        // $('.checkOut-requester').css({"background-color": "#dff0d8", "border": "none"});
        // $('.checkOut-owner').attr('disabled', 'disabled');
      }
    });
  });

  // new claim
  $('#claim-submit').on('click', function(event) {
    event.preventDefault();
    var confirmation = $('#confirmation').val();
    var description = $('#description').val();
    $.ajax({
      type: "POST",
      data: {confirmationNum: confirmation, desc: description},
      url: "/newclaim",
      success: function(data){
        if (data.message == "success") {
          $('.modal-body').text("Thank you. Your claim (" + data.claimNum + ") has been reported to ShareCity team. Please allow up to 24-48 hours for Customer Support to respond to your claim.");
          $('.modal-body').css({'background': '#4692b4', 'margin': '19px', 'border-radius': '10px','color': '#fff'});
        }
      }
    });
  });

  // approve new items
  $('.approveItem-btn').on('click', function() {
    var id = $(this)[0].id;
    $.ajax({
      type: 'PUT',
      data: {itemId: id},
      url: '/approveItem',
      success: function(data) {
        if (data.message == 'success') {
          location.reload();
        }
      }
    });
  });

});
