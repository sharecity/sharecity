// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require jquery-ui/datepicker
//= require jquery-ui.multidatespicker
//= require autocomplete-rails
//= require jquery.raty
//= require ratyrate
//= require jquery.slides.min
//= require toastr_rails
//= require bootstrap
//= require underscore
//= require gmaps/google
//= require message-bus
//= require social-share-button
//= require_tree .


$(document).ready(function() {

  MessageBus.start();
  MessageBus.callbackInterval = 300;

  var sideslider = $('[data-toggle=collapse-side]');
  var sel = sideslider.attr('data-target');
  var sel2 = sideslider.attr('data-target-2');
  sideslider.click(function(event){
      $(sel).toggleClass('in');
      $('body').toggleClass('dis-bk');
      $('.wrapper').toggleClass('dis-bk2');
      $(sel2).toggleClass('out');
  });

  $("a.popup").click(function(e) {
    popupCenter($(this).attr("href"), $(this).attr("data-width"), $(this).attr("data-height"), "authPopup");
    e.stopPropagation(); return false;
    });

    // hover script for the video on the landing page
    $('.landing-video').hover(function toggleControls() {
      if (this.hasAttribute("controls")) {
        this.setAttribute("controls", "controls");
      } else {
        this.removeAttribute("controls");
      }
    });

    $('.avatar-modal-btn').on('click', function() {
      $('#avatarModal').modal('toggle');
    });

    $('.avatar-modal-btn2').on('click', function() {
      $('#avatarModal2').modal('toggle');
    });

    $('#myModalBilling').modal('hide')
    $('#myModalBillingMobile').modal('hide');

    $(".addCard").on('click', function() {
      $('.ccForm').removeClass('hidden');
      $('.resInfo').hide();
    });

    usersIdsBilling = [];
    $('#accepted-reservations').on('click', '.btn_pay', function(e){
      e.preventDefault();
      var btn = $(this);
      var item_id = parseInt( btn.parent().find('.it').attr('data-resItemId'));
      var reservation_id = parseInt( btn.parent().find('.re').attr('data-resId'));
      $('#modalPayment-' + reservation_id ).modal('show');


      // $.ajax('/billings',
      //   { dataType: 'json',
      //     method: 'GET',
      //     success: function(server_response){
      //       console.log(server_response);
      //       $.each(server_response.billings, function(arrID, server_response) {
      //         d = server_response.user.id
      //         usersIdsBilling.push(d);
      //       });
      //     }
      //   }).done(function() {
      //     console.log(usersIdsBilling);
      //       var usr_id = parseInt( $('.currUsId').attr('data-id'));
      //       var checkArray = $.inArray(usr_id, usersIdsBilling);
      //       // 0 means has billings info - 1 means has no billings info
      //       if (checkArray === -1) {
      //         console.log("This user has no billing info");
      //         $('#myModalBilling').modal('show');
      //       } else {
      //         console.log("this user has billing info");
      //         btn.prop('disabled', true).text('Saving...');
      //         // var item_id   = $('.it')[0].id
      //         var item_id = parseInt( $('.it').attr('data-resItemId'));
      //         var reservation_id = parseInt( $('.re').attr('data-resId'));
      //         // var reservation_id = $('.re')[0].id
      //         $.ajax({
      //           type: "PUT",
      //           data: {reservationId: reservation_id, statusItem: "paid"},
      //           url: "/reservations/" +reservation_id+ "/`updateWithPay`",
      //           success: function(res){
      //             $('#accepted-request-' + res.res_id).empty().text('Your payment has been completed. A confirmation email has been sent to you.');
      //           }
      //         });
      //       }
      //     });
    });
    //
    // $('.createCard').on('click', function() {
    //   $.ajax({
    //     type: 'POST',
    //     url: '/billings',
    //     data: {},
    //     success: function(xhr) {
    //       console.log(xhr);
    //       if (xhr.status == true) {
    //         $('.ccForm').addClass('hidden');
    //         $('.resInfo').show();
    //         $('.modal-body').find('.alert-success').show().text(xhr.message);
    //       }
    //     }
    //   });
    // });

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
});
