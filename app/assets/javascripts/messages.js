$(function() {
  $('#new_conversation').hide();
  $('.message-btn').on('click', function(e) {
    e.preventDefault();
    $('#message-alert').empty();
    $('#new_conversation').toggle();
  });
});
