jQuery(function($) {
  //if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
  //  console.log($('.desktop'));
  // $('.desktop').remove();
  //}
  
  // USER
  var currentuser = $('#current-user');
  var user = { 
    current_id: currentuser.attr('data-user_id'),
    current: currentuser.attr('data-username')
  };
  
  var channels = { 
    send: '/conversations/from-'+user.current_id,
    receive: '/conversations/to-'+user.current_id
  };
  
  var convo = $('.user-message');
  var convoInfo = {
    sid: convo.attr('data-sid'), 
    rid: convo.attr('data-rid'),
    cid: convo.attr('data-cid'),
    avatar: convo.attr('data-avatar'),
    sender: function(data) {
      return (user.current_id == data.sid && user.current_id == data.sent) || (data.target == data.sid && data.target == data.sent);
    },
    avatarImg: function(img, type) {
      var dimension = type === 'convo' ? '80px' : '50px'
      return $('<img>').addClass('circle msgImg')
               .attr('src', img)
               .css('width', dimension)
               .css('height', dimension);
    }
  };

  var convoMobile = $('.user-message-mobile');
  var convoInfoMobile = {
    sid: convoMobile.attr('data-sid'), 
    rid: convoMobile.attr('data-rid'),
    cid: convoMobile.attr('data-cid'),
    avatar: convoMobile.attr('data-avatar')
  };

  var time = { 
    addZero: function(num) {
      return num < 10 ? '0' + num : num
    },
    to12: function(time) {
      var hour = time.getHours();
      var min = this.addZero(time.getMinutes());
      if (hour > 12) {
        var hr12 = hour - 12;
        var ampm = 'PM';
      }
      else {
        var hr12 = hour;
        var ampm = 'AM';
      }
      return [hr12, min].join(':') + ' ' + ampm;
    },
    msg: function() { 
      var d = new Date();
      return this.to12(d);
    },
    chat: function() {
      var d = new Date();
      var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
      var month = months[d.getMonth()];
      var day = d.getDate();
      return this.to12(d) + ' - ' + [month, day].join(' ');
    }
  };
  
  var chat = { 
    newMessage: function() {
      $('#new-message-btn').prop('disabled', false); 
      var convo = $('.user-message');
      var msg = convo.find('#new-message-box').val();
      $.ajax('/conversations/messages/new',
        { dataType: 'json',
          method: 'POST',
          data: { body: msg, user: user.current_id, 
                  rid: convo.data('rid'), sid: convo.data('sid'), 
                  cid: convo.data('cid') },
          complete: function(xhr) { console.log(xhr.status); }
        });
    },
    read: function(convo) {
      $.ajax('/conversations/read',
        { dataType: 'json',
          method: 'POST',
          data: { cid: convo },
          complete: function(xhr) {
            if (xhr.status === 200) {
              var count = xhr.responseJSON.unread;
              $('#unread-count, #unread-count-mobile').text( count );
            }
          }
        });
    },
    unread: function(sid, convo) {
      $('#new-message-box').val('');
      if (sid != user.current_id) {
        var count = $('#unread-count').text();
        count = count === '' ? 1 : parseInt(count) + 1;
        $('#unread-count, #unread-count-mobile').text(count > 0 ? count : '');
        $('#convo-'+convo).find('.new-message-alert').show().text('new');
      }
    },
    display: function(data) {
      var allMsgs = $('#c'+data.cid+'-s'+data.sid+'-r'+data.rid);
      var item = $('<div>').addClass('item').appendTo(allMsgs);
      var header = $('<div>').addClass('row header').appendTo(item);
      var username = '<a href="/users/' + data.sid + '">' + data.from + "</a>";
  
      if (convoInfo.sender(data)) {
        var col2 = $('<div>').addClass('col-xs-4 col-sm-2').appendTo(header);
        var col10 = $('<div>').addClass('list col-xs-8 col-sm-10 listPadding').appendTo(header);
        $('<div>').addClass('item')
          .addClass('conv-sender')
          .text(data.body)
          .appendTo(col10);
        convoInfo.avatarImg(data.avatar, 'msg').appendTo(col2);
        $('<strong>').html([username, time.msg()].join(' ')).appendTo(col2);
      }
      else {
        var col10 = $('<div>').addClass('list col-xs-8 col-sm-10 listPadding').appendTo(header);
        var col2 = $('<div>').addClass('col-xs-4 col-sm-2').appendTo(header);
  
        convoInfo.avatarImg(data.avatar, 'msg').appendTo(col2);
        $('<strong>').html([username, time.msg()].join(' ')).appendTo(col2);
        $('<div>').addClass('item')
          .addClass('conv-recipient')
          .text(data.body)
          .appendTo(col10);
      }
    }
  };

  // MESSAGE TEXTAREA
  var hidden = $('.hidden-form-conv');
  var content;
  var allMsgs = $('.all-messages');
  var chatHeight = allMsgs.height();

  $('.msg-render').on('keypress keyup paste', '.form-conv', function() {
    content = $(this).val().replace(/\n/g, '<br>');
    hidden.html(content);
    $(this).css('height', hidden.height());
    allMsgs.css('height', chatHeight - $(this).height);
  });

  // SUBSCRIPTIONS
  MessageBus.subscribe(channels.send, function(data) {
    console.log(data)
    if (data.message) { 
      chat.display(data);
      chat.unread(data.sent, data.cid);
    }
    else { 
      displayConvo(data);
    }
  });

  MessageBus.subscribe(channels.receive, function(data) {
    if (data.message) { 
      chat.display(data);
      chat.unread(data.sent, data.cid);
    }
    else { 
      displayConvo(data);
    }
  });

  // NEW MESSAGES
  $('.msg-render').on('click', '#new-message-btn', function() {
    $(this).prop('disabled', true);
    chat.newMessage();
  });

  // NEW CONVERSATION
  $('.user-message').on('click', '#new-message-btn', function() {
    var data = { sid: convoInfo.sid, rid: convoInfo.rid, body: $('#new-message-box').val() };
    $('#new_conversation').hide();
    newConvo(data);
    $('#message-alert').text('Message sent').css('font-weight', 'bold');
    $('#new-message-box').val('');
  });

  // NEW CONVERSATION - MOBILE
  $('#myMobileModal').on('click', '#new-message-btn-mobile', function() {
    var data = { sid: convoInfoMobile.sid, rid: convoInfoMobile.rid, body: $('#new-message-box-mobile').val() };
    newConvo(data);
    $('#message-alert-mobile').text('Message sent').css('font-weight', 'bold');
    $('#new-message-box-mobile').val('');
  });


  function newConvo(data) {
    $.ajax('/conversations',
      { dataType: 'json',
        method: 'POST',
        data: data,
        complete: function(xhr) { console.log(xhr.status); }
      });
  }

  function displayConvo(data) {
    var convos = $('#conversations');
    var row = $('<div>').addClass('row row-conv convNew').prependTo(convos);
    var col2 = $('<div>').addClass('col-xs-12 col-sm-2').appendTo(row);
    var col10 = $('<div>').addClass('hidden-xs col-sm-10 col-conv')
                  .attr('id', 'convo-' + data.cid)
                  .appendTo(row);
    var nameDiv = $('<span>').addClass('name').css('min-width', '120px').appendTo(col10);

    chat.unread(data.sid, data.cid);
    convoInfo.avatarImg(data.avatar, 'convo').addClass('dynamic-conv').appendTo(col2);
    $('<a>').addClass('name-conv')
      .attr('href', '/conversations/' + data.cid + '/messages')
      .addClass('dynamic-conv')
      .attr('data-cid', data.cid)
      .css('font-weight', 'bold')
      .text(data.from)
      .appendTo(nameDiv);
    $('<div>').addClass('new-message-alert')
      .text('new')
      .appendTo(nameDiv);
    nameDiv.append('<br/>');
    $('<div>').addClass('badge').text( time.chat() ).appendTo(nameDiv);
  }

  // MARK UNREAD MESSAGES AS READ
  $('#conversations').on('click', '.name-conv, .msgImg', function() {
    $(this).parents('.row-conv').siblings().css({'background': '#fff', 'border-left': 'none'});
    $(this).parents('.row-conv').css({'background': '#ddd', 'border-left': '4px solid #4893b3'});
    $(this).parent().siblings('.new-message-alert').hide();

    var convo = $(this).attr('data-cid');
    chat.read(convo);
  });

  $('#conversations').on('click', '.dynamic-conv', function(e) {
    e.preventDefault();
    var link = $(this).attr('href');
    var cid = $(this).attr('data-cid');
    $('#convo-'+cid).find('.new-message-alert').hide();

    $.ajax(link + '.js',
      { complete: function(xhr) { console.log(xhr.status); }
      });

    chat.read(cid);
  });
});
