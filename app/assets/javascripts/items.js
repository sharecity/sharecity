jQuery(function($) {
  $('#copy-btn').click(function(){
    $('#shareLink').copyme();
  });

  $.fn.copyme = function() {
  	this.select();
  	$(this).focus();
    document.execCommand("copy");
    document.getSelection().removeAllRanges();
	};
  // $("#search-form").submit(function() {
  //   $.get(this.action, $(this).serialize(), null, "script");
  //   return false;
  // });

//   $("#search-form").submit(function() {
//
//   $("#search").addClass("loading"); // show the spinner
//   var form = $("#search-form"); // grab the form wrapping the search bar.
//
//   var url = "/items"; //  action.
//   var formData = form.serialize(); // grab the data in the form
//   $.get(url, formData, function(html) { // perform an AJAX get
//
//     $("#search").removeClass("loading"); // hide the spinner
//
//     $(".mm").html(html); // replace the "results" div with the results
//
//   });
//   return false;
// });


  $('#item-slides').slidesjs({
    navigation: { active: false },
    pagination: { active: true }
  });
  $('#item-slides-mobile').slidesjs({
    navigation: { active: false },
    pagination: { active: true }
  });

  $('#step-form-submit').hide();
  $('.step-closed').hide();

  $('#multistep-selectable').selectable({
    selected: function() {
      var visible = $('.ui-selected:visible');
        $.ajax('/items/share',
          { dataType: 'json',
            type: 'POST',
            data: { share: visible.val() },
            complete: function(xhr) { console.log(xhr.status) }
          });
    }
  });
  $('.ui-widget-content:first-child').addClass('ui-selected');
  function formStart() {
    var prevField = $('.step-active').prev('.step-closed');
    if (prevField.length > 0) {
      $('#step-form-prev').show()
    }
    else {
      $('#step-form-prev').hide();
    }
  }

  function formEnd() {
    var nextField = $('.step-active').next('.step-closed');
    if (nextField.length === 0) {
      $('#step-form-next').hide();
      $('#step-form-submit').show();
    }
    else {
      $('#step-form-next').show();
      $('#step-form-submit').hide();
    }
  }

  formStart();

  $('#step-form-next').on('click', function() {
    var next = $('.step-active').next('.step-closed');
    next.addClass('step-active').removeClass('step-closed');
    next.prev('.step-active').addClass('step-closed').removeClass('step-active');

    var nextTxt = $('.orange-text').next('.inactive-text');
    nextTxt.addClass('orange-text').removeClass('inactive-text');
    nextTxt.prev('.orange-text').addClass('inactive-text').removeClass('orange-text');
    formStart();
    formEnd();
    $('.step-closed').hide();
    $('.step-active').show();
  });

  $('#step-form-prev').on('click', function() {
    var prev = $('.step-active').prev('.step-closed')
    prev.addClass('step-active').removeClass('step-closed');
    prev.next('.step-active').addClass('step-closed').removeClass('step-active');

    var prevTxt = $('.orange-text').prev('.inactive-text');
    prevTxt.addClass('orange-text').removeClass('inactive-text');
    prevTxt.next('.orange-text').addClass('inactive-text').removeClass('orange-text');
    formStart();
    formEnd();
    $('.step-closed').hide();
    $('.step-active').show();
  });

  // Mobile

  $('#step-form-submit-mobile').hide();
  $('.step-closed-mobile').hide();

  function formStartMobile() {
    var prevField = $('.step-active-mobile').prev('.step-closed-mobile');
    if (prevField.length > 0) {
      $('#step-form-prev-mobile').show()
    }
    else {
      $('#step-form-prev-mobile').hide();
    }
  }
  function formEndMobile() {
    var nextField = $('.step-active-mobile').next('.step-closed-mobile');
    if (nextField.length === 0) {
      $('#step-form-next-mobile').hide();
      $('#step-form-submit-mobile').show();
    }
    else {
      $('#step-form-next-mobile').show();
      $('#step-form-submit-mobile').hide();
    }
  }


  formStartMobile();

  $('#step-form-next-mobile').on('click', function() {
    var next = $('.step-active-mobile').next('.step-closed-mobile');
    next.addClass('step-active-mobile').removeClass('step-closed-mobile');
    next.prev('.step-active-mobile').addClass('step-closed-mobile').removeClass('step-active-mobile');

    var nextTxt = $('.orange-text-mobile').next('.inactive-text-mobile');
    nextTxt.addClass('orange-text-mobile').removeClass('inactive-text-mobile');
    nextTxt.prev('.orange-text-mobile').addClass('inactive-text-mobile hidden').removeClass('orange-text-mobile');
    formStartMobile();
    formEndMobile();
    $('.step-closed-mobile').hide();
    $('.step-active-mobile').show();
  });

  $('#step-form-prev-mobile').on('click', function() {
    var prev = $('.step-active-mobile').prev('.step-closed-mobile')
    prev.addClass('step-active-mobile').removeClass('step-closed-mobile');
    prev.next('.step-active-mobile').addClass('step-closed-mobile').removeClass('step-active-mobile');

    var prevTxt = $('.orange-text-mobile').prev('.inactive-text-mobile');
    prevTxt.addClass('orange-text-mobile').removeClass('inactive-text-mobile hidden');
    prevTxt.next('.orange-text-mobile').addClass('inactive-text-mobile').removeClass('orange-text-mobile');
    formStartMobile();
    formEndMobile();
    $('.step-closed-mobile').hide();
    $('.step-active-mobile').show();
  });

});
