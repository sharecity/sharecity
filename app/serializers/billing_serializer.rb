class BillingSerializer < ActiveModel::Serializer
  attributes :id, :address, :city, :country, :postal_code
  has_one :user
end
