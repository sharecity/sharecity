class MainController < ApplicationController
  def index
  end
  def contact
    @contacts = request.env['omnicontacts.contacts']
    respond_to do |format|
        format.html
    end
  end
  def invite
    emails = params[:emails]
    emails.each do |email|
      invitee = email
      UserMailer.invite(current_user, invitee).deliver_now
      redirect_to home_path, notice: 'Thank you for your invitation'
    end
  end
end
