class ReviewsController < ApplicationController
  def new
    @review = Review.new
  end

  def create
    @review       = Review.new review_params
    @review.user  = current_user
    if @review.save
      redirect_to @review.item
    end
  end

  def show
    @review = Review.find params[:id]
  end

  def index
    @reviews = Review.all
  end

  def destroy
    @review = Review.find params[:id]
    @item = @review.item
    @review.destroy
    redirect_to @item
  end

  private

  def review_params
    params.require(:review).permit(:comment, :item_id, :user_id)
  end
end
