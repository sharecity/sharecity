class MessagesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :code_require!
  before_action :find_conversation, except: :new_message

  def show
    @messages = @conversation.messages.order(:created_at)
    @message = @messages.new

    respond_to do |format|
      format.html { render :show_messages }
      format.js { render :show_messages }
    end
  end

  def new_message
    @conversation = Conversation.find(params[:cid].to_i)
    @message = @conversation.messages.create(
      user_id: params[:user].to_i,
      body: params[:body]
      )
    data = { body: params[:body], from: @message.user.first_name.capitalize, target: params[:sid],
             sid: params[:sid].to_i, sent: current_user.id, rid: params[:rid].to_i, 
             cid: @conversation.id, avatar: current_user.img, message: true }
    MessageBus.publish "/conversations/to-#{params[:sid]}", data 
    MessageBus.publish "/conversations/to-#{params[:rid]}", data 
    render json: @message
  end

 # def index
 #   @messages = @conversation.messages
 #   if @messages.last
 #     # binding.pry
 #     if @messages.last.user_id != current_user.id
 #       @messages.last.read = true;
 #     end
 #   end
 #   @message = @conversation.messages.new
 #   respond_to do |format|
 #     # format.json { render json: {message: @message, answers: @conversation.messages} }
 #     format.html { render "/conversations/index" }
 #     format.js { render :show_messages }
 #   end
 # end


  private

    def message_params
      params.require(:message).permit(:body, :user_id, :conversation_id, :reservation_id, :read)
    end
    def find_conversation
      @conversation = Conversation.find params[:conversation_id]
    end
end
