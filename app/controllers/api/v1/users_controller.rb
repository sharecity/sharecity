class Api::V1::UsersController < Api::V1::BaseController
  # before_filter :authenticate_user!, only: [:show, :update, :destroy]
  # before_filter :restrict_access

  skip_before_filter :verify_authenticity_token

  def index
    render json: User.all
  end
  def show
    user = User.find(params[:id])
    render json: user
  end

  def create
    user = User.new user_params
    if user.save
      render status: 201, json: {
        message: "Successfully created User.",
        api: user
      }.to_json
    else
      render status: 422, json: {
        errors: user.errors
      }.to_json
    end
  end

  def update
    user = User.find(params[:id])

    if user.update(user_params)
      render status: 200, json: {
        message: "Successfully Updated User.",
        api: user
      }.to_json
    else
      render status: 422, json: {
        errors: user.errors
      }.to_json
    end
  end

  def destroy
    user = User.find(params[:id])
    user.destroy
    head 204
  end

  # def index
  #   render json: User.where('owner_id = ?', @user.id)
  #   # users = User.find(params[:item_id]).items
  #   # if params[:item_id]
  #   #   users = User.find(params[:item_id]).items
  #   # else
  #   #   users = User.all.order(created_at: :asc)
  #   # end
  #
  #   # users = apply_filters(users, params)
  #   #
  #   # users = paginate(users)
  #   #
  #   # users = policy_scope(users)
  #   #
  #   # render(
  #   #   json: ActiveModel::ArraySerializer.new(
  #   #     users,
  #   #     each_serializer: Api::V1::UserSerializer,
  #   #     root: 'users',
  #   #     meta: meta_attributes(users)
  #   #   )
  #   # )
  # end
  #
  # def show
  #
  #   @user = User.find(params[:id])
  #   render json: @user
  # #   # authorize user
  # #
  # #   render(json: Api::V1::UserSerializer.new(user).to_json)
  # end
  #
  # def create
  #   user = User.new(create_params)
  #   return api_error(status: 422, errors: user.errors) unless user.valid?
  #
  #   user.save!
  #   user.activate
  #
  #   render(
  #     json: Api::V1::UserSerializer.new(user).to_json,
  #     status: 201,
  #     location: api_v1_user_path(user.id)
  #   )
  # end
  #
  # def update
  #   user = User.find(params[:id])
  #   authorize user
  #
  #   if !user.update_attributes(update_params)
  #     return api_error(status: 422, errors: user.errors)
  #   end
  #
  #   render(
  #     json: Api::V1::UserSerializer.new(user).to_json,
  #     status: 200,
  #     location: api_v1_user_path(user.id),
  #     serializer: Api::V1::UserSerializer
  #   )
  # end
  #
  # def destroy
  #   user = User.find(params[:id])
  #   authorize user
  #
  #   if !user.destroy
  #     return api_error(status: 500)
  #   end
  #
  #   head status: 204
  # end
  #

  

  private

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end
  #
  # def create_params
  #   params.require(:user).permit(
  #     :email, :password, :password_confirmation, :name
  #   ).delete_if{ |k,v| v.nil?}
  # end
  # def update_params
  #   create_params
  # end
  #
  # def restrict_access
  #     authenticate_or_request_with_http_token do |token, options|
  #     ApiKey.exists?(access_token: token)
  #   end
  # end

  # def restrict_access
  #   api_key = User.find_by_authentication_token(params[:authentication_token])
  #   head :unauthorized unless api_key
  # end
end
