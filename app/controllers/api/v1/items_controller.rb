class Api::V1::ItemsController < Api::V1::BaseController
  skip_before_filter :verify_authenticity_token

  def index
    Rails.logger.info "Current user: #{current_user.inspect}"
    render json: Item.all
  end

  def show
    item = current_user.items.find params[:id]
    render json: item
  end

  def create
    item = current_user.items.new item_params
    if item.save
      render status: 200, json: {
        message: "Successfully created Item.",
        item: item
      }.to_json
    else
      render status: 422, json: {
      errors: item.errors
    }.to_json
    end
  end

  def update
    item = current_user.items.find params[:id]
    if item.update item_params
      render status: 200, json: {
        message: "Successfully Updated Item.",
        item: item
      }.to_json
    else
      render status: 422, json: {
      message: "The Item could not be Updated.",
    }.to_json
    end
  end

  def destroy
    item = current_user.items.find params[:id]
    item.destroy
    render status: 200, json: {
      message: "Successfully Deleted Item.",
      item: item
    }.to_json
  end


  private

  def item_params
    params.require(:item).permit(:name)
  end

end
