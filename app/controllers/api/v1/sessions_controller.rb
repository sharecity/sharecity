class Api::V1::SessionsController < Api::V1::BaseController


  def create
    @user = User.find_by_email params[:email]
    if @user && @user.authenticate(params[:password])
      if @user.email_confirmed?
          api_token = ApiKey.generate_access_token
          render status: 200, json: {
            message: "Successfully Logged in.",
            api_token: api_token
          }.to_json
      else
        render status: 422, json: {
          message: "Please confirm your account!",
        }.to_json
      end
    end
  end

  private

  def create_params
    params.require(:user).permit(:email, :password)
  end

end
