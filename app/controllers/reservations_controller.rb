class ReservationsController < ApplicationController
  before_filter :authenticate_user!
  #before_filter :billing_info, only:[:create]

  def index
    @items          = current_user.items
    @allItems       = Item.all
    @checkingInReq  = current_user.reservations.readyForCheckInRequester.order(created_at: :desc)
    @checkingOutReq = current_user.reservations.readyForCheckOutRequester.order(created_at: :desc)
    @checkingInOwn  = Reservation.readyForCheckInOwner.order(created_at: :desc)
    @checkingOutOwn = Reservation.readyForCheckOutOwner.order(created_at: :desc)
    @complete       = Reservation.where("user_id = ? AND checkedin_owner = ? AND checkedout_owner = ? AND checkedout_requester = ? AND checkedin_requester =?", current_user.id, 'yes', 'yes', 'yes', 'yes')
    @claims       = Claim.where("user_id = ?", current_user.id)
  end

  def preload
		item             = Item.find params[:item_id]
		today            = Date.today
    stat             = "requested"
    # unavailableDates = UnavailableDate.where("item_id = ?", item)
    # undates          = item.unavailable_dates
    reservations     = item.reservations.where("start_date >= ? OR end_date >= ?", today, today) && item.reservations.where.not("status >= ?", stat)
    # render json: undates
		render json: reservations
	end

	def preview
		start_date = Date.parse(params[:start_date])
		end_date = Date.parse(params[:end_date])

		output = {
			conflict: is_conflict(start_date, end_date),
      unavailableDates_conflict: is_conflict2(start_date, end_date)
		}

		render json: output
	end

  def newclaim
    claim_num = "#{current_user.id}-#{SecureRandom.hex(3)}"
    if @claim = Claim.create!(confirmation_number: params[:confirmationNum], note: params[:desc], user_id: current_user.id, claim_num: claim_num)
      UserMailer.new_claim(current_user, @claim).deliver_now
      result = {message: "success", claimNum: @claim.claim_num}
      render json: result
    else
      result = {message: "error"}
      render json: result
    end
  end

	def create
    @reservation      = Reservation.new reservation_params
    @reservation.user = current_user
    @reservation.save

    item = @reservation.item

    data = { requester: current_user.first_name, requester_id: current_user.id,
             owner_id: item.user_id, owner: item.user.full_name,
             start: @reservation.start_date.strftime('%B %d'),
             end: @reservation.end_date.strftime('%B %d, %Y'), item_id: item.id,
             item: item.name, res_id: @reservation.id }

    MessageBus.publish "/received-item-requests/user-#{item.user.id}", data
    MessageBus.publish "/your-item-requests/user-#{current_user.id}", data.merge(cancel_path: item_reservation_path(item, @reservation, {status: 'cancelled-request'}) )

    if @reservation.item.price_status == "0"
      # if Billing.where('user_id =?', current_user.id).exists?
      UserMailer.requestPending(@reservation.item.user, current_user, @reservation.id).deliver_now
      render json: { status: 'success' }
      # else
        # session[:return_to] ||= request.referer
        # render :json => { :message => "NeedsBilling"  }
        # render :js => "window.location = '/billings/new'"
        # render json: { status: no_billing_info}
        # redirect_to new_billing_path, notice: "please fill up the form in order to continue"
      # end
    else
      # UserMailer.requestPending(@reservation.item.user, current_user, @reservation.id).deliver_now
      render json: @reservation.as_json(
      include: {
        user: { only: [:first_name, :last_name]},
        item: { include: {user: { include: {followers: {only: [:id]}} }},
        only: [:name, :user_id, :sharing]} } )
    end
	end

  def update
    @reservation = Reservation.find params[:id]
    owner             = @reservation.item.user
    requester         = @reservation.user
    amount            = (@reservation.item.price).to_i

    if @reservation.update status: params[:status]
      if params[:status] == "approved-owner"

        data = { status: 'accepted', requester: requester.first_name,
                 start: @reservation.start_date.strftime('%B %d'),
                 end: @reservation.end_date.strftime('%B %d, %Y'),
                 price: @reservation.total.to_f > 0 ? @reservation.total : 0,
                 item: @reservation.item.name, res_id: @reservation.id,
                 item_id: @reservation.item_id }
        owner_actions = data.merge(role: 'owner', cancel_path: item_reservation_path(@reservation.item, @reservation, {status: 'cancelled-owner'}) )
        requester_actions = data.merge(role: 'requester', cancel_path: item_reservation_path(@reservation.item, @reservation, {status: 'cancelled'}), pay_path: item_reservation_path(@reservation.item, @reservation, {status: 'paid'}) )

        MessageBus.publish "/accepted-reservations/user-#{owner.id}", owner_actions
        MessageBus.publish "/accepted-reservations/user-#{requester.id}", requester_actions
        UserMailer.itemApproval(owner, requester, @reservation.id).deliver_now
      # elsif params[:checkedin_owner] == "yes"
      #   redirect_to reservations_path, notice: "Checked In"
      elsif params[:status] == 'rejected-owner'
        data = { status: 'rejected', res_id: @reservation.id, item: @reservation.item.name }
        MessageBus.publish "/accepted-reservations/user-#{owner.id}", data
        MessageBus.publish "/accepted-reservations/user-#{requester.id}", data
        UserMailer.itemRejected(owner, requester).deliver_now
      else
        data = { status: 'cancelled', item: @reservation.item.name, res_id: @reservation.id }
        MessageBus.publish "/accepted-reservations/user-#{owner.id}", data
        MessageBus.publish "/accepted-reservations/user-#{requester.id}", data
      end

      render json: { res_id: @reservation.id }
      # elsif @reservation.status == "canceled"
      #   UserMailer.itemRejected(owner, requester).deliver_now
      # end
    else
      flash[:alert] = "See error(s) below..."
      redirect_to home_path
    end
  end

  def updateWithPay
    @reservation = Reservation.find params[:id]
    item         = @reservation.item
    amount       = @reservation.total.to_i
    owner        = @reservation.item.user
    requester    = @reservation.user
    if item.price_status == "1"
      begin
        reservationTransactions = Transaction.new( :amount => amount,
        :user_id => @reservation.item.user.id,
        :confirmation_id => "#{@reservation.item.user.id}#{Time.now.to_i}",
        :item_id => @reservation.item.id,
        :fees => "0",
        # :payout => payout,
        # :damage_deposit => item.damage_deposit,
        # :deposit_charge => deposit ? deposit[:id] : nil,
        :charge => nil,
        :last4 => nil,
        :reservation_id => @reservation.id )
        reservationTransactions.save
        Reservation.update(@reservation.id, :status => "paid")
        UserMailer.bookingConfirmed(owner, @reservation.id, reservationTransactions).deliver_now
      rescue => e
        render json: {status: false, error: e.message}
      end
    else
      begin
        charge = Stripe::Charge.create(
        :customer    => current_user.customer_id,
        :amount      => amount * 100,
        :description => 'Rails Stripe customer',
        :currency    => 'cad'
        )
        # if item.damage_deposit > 0
        #   deposit = Stripe::Charge.create(
        #            customer: current_user.customer_id,
        #            amount: (item.damage_deposit * 100).to_i,
        #            capture: false,
        #            description: "Damage deposit for #{item.name}",
        #            currency: 'cad' )
        # end
        reservationTransactions = Transaction.new( :amount => amount,
        :user_id => @reservation.item.user.id,
        :confirmation_id => "#{@reservation.item.user.id}#{Time.now.to_i}",
        :item_id => @reservation.item.id,
        :fees => charge[:amount] / 100,
        # :payout => payout,
        # :damage_deposit => item.damage_deposit,
        # :deposit_charge => deposit ? deposit[:id] : nil,
        :charge => charge[:id],
        :last4 => charge[:source][:last4],
        :reservation_id => @reservation.id )
        reservationTransactions.save
        Reservation.update(@reservation.id, :status => "paid")
        UserMailer.bookingConfirmed(owner, @reservation.id, reservationTransactions).deliver_now
      rescue Stripe::RateLimitError => e
        render json: {status: false, error: e.message}
      rescue Stripe::InvalidRequestError => e
        render json: {status: false, error: e.message}
      rescue Stripe::AuthenticationError => e
        render json: {status: false, error: e.message}
      rescue Stripe::APIConnectionError => e
        render json: {status: false, error: e.message}
      rescue Stripe::StripeError => e
        render json: {status: false, error: e.message}
      rescue Stripe::CardError => e
        # redirect_to home_path, alert: e.message
        render json: {status: false, error: e.message}
      rescue => e
        render json: {status: false, error: e.message}
      end
    end

    data = { status: params[:statusItem], item: @reservation.item.name, res_id: @reservation.id }
    MessageBus.publish "/accepted-reservations/user-#{requester.id}", data
    MessageBus.publish "/accepted-reservations/user-#{owner.id}", data

    render json: { status: @reservation.status, res_id: @reservation.id }
  end

  def updateCheck
    @reservation = Reservation.find params[:id]
    amount       = @reservation.total.to_i
    if params[:checkingInOwner] == "yes"
      @reservation.update :checkedin_owner => params[:checkingInOwner]
      flash[:notice] =  "Checked In by Owner"
    elsif params[:checkingInRequester] == "yes"
      @reservation.update :checkedin_requester => params[:checkingInRequester]
      flash[:notice] = "Checked In by Requester"
    elsif params[:checkingOutRequester] == "yes"
      @reservation.update :checkedout_requester => params[:checkingOutRequester]
      flash[:notice] = "Checked Out by Requester"
    elsif params[:checkingOutOwner] == "yes"
      @reservation.update :checkedout_owner => params[:checkingOutOwner]
      flash[:notice] = "Checked Out by Owner"
    end
    # IF REQUESTER & OWNER BOTH CHECKED OUT --> REFUND DAMAGE DEPOSIT
    # begin
    #   if @reservation.all_checked_out?
    #     deposit = @reservation.get_transaction
    #     if deposit && deposit.refundable?
    #       deposit_charge_id = deposit.deposit_charge
    #       deposit_refund = Stripe::Refund.create(charge: deposit_charge_id)
    #       deposit.update(deposit_refund: deposit_refund[:id])
    #     end
    #   end
    #   render json: @reservation
    #  rescue Stripe::CardError => e
    #   redirect_to reservations_path, alert: e.message
    # rescue Stripe::InvalidRequestError => e
    #   redirect_to reservations_path, alert: e.message
    # end

    # IF REQUESTER & OWNER BOTH CHECKED IN --> TRANSFER RENT TO THE OWNER
    a=(amount * 0.15)
    b= amount - a
    begin
      if @reservation.all_checked_in?
        Stripe::Transfer.create(
          :amount => (amount * 100).to_i,
          :currency => 'cad',
          :destination => @reservation.item.user.stripe_uid,
          :application_fee => (a * 100).to_i,
        )
      end
    rescue Stripe::CardError => e
      redirect_to home_path, alert: e.message
    rescue Stripe::InvalidRequestError => e
      redirect_to home_path, alert: e.message
    end
  end

	private
		def billing_info
		   if Billing.where('user_id =?', current_user.id).exists?
			redirect_to new_billing_path, notice: "please fill up the form in order to continue"
	 	   end
		end

		def is_conflict(start_date, end_date)
			item = Item.find(params[:item_id])

			check = item.reservations.where("? < start_date AND end_date < ?", start_date, end_date)
			check.size > 0? true : false
		end
    def is_conflict2(start_date, end_date)
			item = Item.find(params[:item_id])

			check = item.unavailable_dates.where("? < date AND date < ?", start_date, end_date)
			check.size > 0? true : false
		end

		def reservation_params
			params.require(:reservation).permit(:start_date, :end_date, :price, :total, :item_id, :item_owner_id, :status, :checkedin_owner, :checkedout_owner, :checkedin_requester, :checkedout_requester)
		end

end
