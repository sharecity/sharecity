class SessionsController < ApplicationController
  before_filter :disable_nav, only: [:new]

  def new
  end

  def create
    # @user.referral_code = session[:referral]
    @user = User.from_omniauth(env["omniauth.auth"])
    @user.referral_code = session[:referral]
    session[:user_id] = @user.id
    @user.increment_login_count
    # byebug
    if session[:referral]
      @user.referral_code = session[:referral]
      if @user.save && @user.referral_code == "linksharing" && @user.sign_in_count == 1
        UserMailer.welcomeLinksharing(@user).deliver_now
      elsif @user.save && @user.referral_code == "friends" && @user.sign_in_count == 1
          UserMailer.welcome(@user).deliver_now
      elsif @user.save && @user.referral_code == "socialmedia" && @user.sign_in_count == 1
          UserMailer.welcomeLinksharing(@user).deliver_now
      elsif @user.save && @user.referral_code == "bloggers" && @user.sign_in_count == 1
          UserMailer.welcomeLinksharing(@user).deliver_now
      elsif @user.save && @user.referral_code == "schools" && @user.sign_in_count == 1
          UserMailer.welcomeLinksharing(@user).deliver_now
      elsif @user.sign_in_count == 1
          UserMailer.welcomeLinksharing(@user).deliver_now
      else
        UserMailer.welcomeLinksharing(@user).deliver_now
      end
    end
    # redirect_to dashboard_index_path, notice: "Logged In"
    redirect_to welcome_path  # This is for The landing page
  end

  def create_email
    @user = User.find_by_email params[:email]
    if @user && @user.authenticate(params[:password])
      session[:user_id] = @user.id
      if @user.code_active == 'active'
        redirect_to home_path, notice: "Logged in"
      else
        redirect_to welcome_path, notice: "Logged in"
      end
      # if @user.email_confirmed?
      #     session[:user_id] = @user.id
      #   redirect_to welcome_path
      #   # , notice: "Logged in"
      # else
      #   flash[:alert] = 'Please activate your account by following the
      #   instructions in the account confirmation email you received to proceed'
      #   render :new
      # end
    else
        flash[:alert] = 'Invalid email/password combination' # Not quite right!
        render :new
    end
  end


  def destroy
    session[:user_id] = nil
    session[:referral] = nil
    redirect_to root_path, notice: "Logged out!"
  end

  def failure
    redirect_to root_path
  end

  private

  def userExists

  end

end
