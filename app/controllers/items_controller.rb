class ItemsController < ApplicationController
  include ActiveSupport::NumberHelper
  before_filter :authenticate_user!
  # before_filter :code_require!
  before_action :check_if_item_approved, only: [:show]
  before_action :check_item_shared, only: [:show]
  autocomplete :tag, :name, full: true

  def new
    @item = Item.new
    @tags = Tag.all
    @item_attachment = @item.item_attachments.build
    @unavailable_date = @item.unavailable_dates.build
  end

  def create
    @tags = Tag.all
    @item = Item.new item_params
    @item.user = current_user
    if params[:item][:price_status] == "1"
      params[:item][:price] = "0.00".to_f
    end
    if @item.save
      if params[:item_attachments]
        params[:item_attachments]['image'].each do |a|
          @item_attachment = @item.item_attachments.create!(:image => a)
        end
      else
        @image = "/images/missing.png"
        @item_attachment = @item.item_attachments.create!(:image => @image, :item_id => @item.id)
      end
      if params[:item]['unavailable_dates_attributes']['0']['date']
	       unavailableDates =  params[:item]['unavailable_dates_attributes']['0']['date'].split(',')
         unavailableDates.each do |udate|
		         @unavailable_date = @item.unavailable_dates.create!(date: udate)
	       end
      end
      data = { item_id: @item.id, img: @item.item_attachments.first.image_url,
               item: @item.name, desc: @item.desc, tags: @item.tags.pluck(:name),
               price: number_to_currency(@item.price), owner_id: @item.user_id,
               owner: @item.user.first_name, share: @item.sharing,
               postal: @item.postal_code }

      # UserMailer.itemPreApproval(@reservation.item.user, current_user, @reservation.id).deliver_now
      # if data[:share] == '0'
      #   MessageBus.publish '/item-feed', data
      # else
      #   # follower channels
      # end

      redirect_to item_path(@item), notice: "Item Successfully created!"
    else
      flash[:alert] = "See error(s) below..."
      @item_attachment = @item.item_attachments.build
      render :new
    end
  end

  def approveItem
    item   = Item.find params[:itemId]
    if item.update(approved: true)
      render json: {message: 'success'}
    else
      render json: {message: 'error'}
    end
  end

  def preloadDates
    item   = Item.find params[:item_id]
    undates = UnavailableDate.where('item_id = ?', item)
    render json: undates
  end

  def item_preview
    @item = Item.find params[:id]
  end

  def share
    session[:share] = params[:share]
    render json: {message: 'Added'}
  end

  def edit
    @item = Item.find params[:id]
    if @item.item_attachments.count == 0
      @item_attachment = @item.item_attachments.build
    end
    @item_attach = ItemAttachment.where(item_id: @item.id )
  end

  def update
    @item = Item.find params[:id]
    @item.approved = false
    if @item.update item_params
      if params[:item_attachments]
        params[:item_attachments]['image'].each do |a|
          @item_attachment = @item.item_attachments.create!(:image => a, :item_id => @item.id)
        end
      end

      unavailableDates =  params[:item]['unavailable_dates_attributes'].split(',')
      unavailableDates.each do |whatever|
        date = UnavailableDate.where("date = ? AND item_id = ?", whatever, @item.id)
        if date.present?
          date.delete_all
        else
          @unavailable_date = UnavailableDate.create!(date: whatever, item_id: @item.id)
        end
      end

      #  if params[:item]['unavailable_dates_attributes']
      #       unavailableDates =  params[:item]['unavailable_dates_attributes']["0"]["date"]
		  #       # unavailableDates.each do |key, value|
      #    	 #  unavailableDates.each do |udate|
      #         byebug
      #          if unavailableDates.present?
      #           #  unavaId           = params[:item]['unavailable_dates_attributes']['0']['id']
      #            @unavailable_date_de = UnavailableDate.find(params[:item]['unavailable_dates_attributes']['0']['id']).delete
      #            byebug
      #          else
      #            byebug
      #            @unavailable_date = @item.unavailable_dates.create!(date: unavailableDates, item_id: @item.id)
      #            byebug
      #           #  @unavailable_date = @item.unavailable_dates.create!(date: udate)
      #          end
      #       # nd
      #  end
      redirect_to item_path(@item), notice: "Item was successfully updated!"
    else
      flash[:alert] = "See error(s) below..."
      render :edit
    end
  end

  def destroy
    @item = Item.find params[:id]
    Tagging.where(item_id: @item).delete_all
    ItemAttachment.where(item_id: @item).delete_all
    UnavailableDate.where(item_id: @item).delete_all
    # Transaction.where(item_id: @item).delete_all
    Review.where(item_id: @item).delete_all
    @item.destroy
    redirect_to home_path, notice: "Item was successfully deleted!"
  end

  def index
    if params[:search] == ""
      @items = Item.where(approved: true)
    else
      @items = Item.search(params[:search])
      if @items.empty?
        @itemsByTitle = []
        key_words = params[:search].to_s.split(", ")
        cc = key_words.map { |s| "'#{s.slice(0,1).capitalize + s.slice(1..-1)}'" }.join(', ')
        key_words.each do |d|
        @itemsByTitle << Item.where("name like ?", "%#{d.capitalize}%")
        # byebug
        end
        @v = @itemsByTitle.flatten
      end
      # redirect_to noItem_path
    end
  end

  def show
    @item_attachments = @item.item_attachments.where('image IS NOT NULL')
    @billing = Billing.new
    stat  = "paid"
    # @booked           = Reservation.where("item_id = ? AND user_id = ? AND status = ?", @item.id, current_user.id, stat).present? if current_user
    # byebug
    @bookedAndChecked = Reservation.where("item_id = ? AND user_id = ? AND status = ? AND checkedin_owner =? AND checkedout_owner =? AND checkedout_requester =? AND checkedin_requester=?", @item.id, current_user.id, stat, "yes", "yes", "yes", "yes").present? if current_user
    @reviews          = @item.reviews
    @hasReview        = @reviews.find_by(user_id: current_user.id) if current_user
    respond_to do |format|
      format.html { render }
      format.json { render json: {item: @item} }
      format.xml  { render xml: @item }
    end
  end

  def autocomplete
    @tags = Tag.order(:name).where("name LIKE ?", "%#{params[:term]}%")
    respond_to do |format|
      format.html
      format.json {
        render json: @tags.map(&:name)
      }
    end
  end

  def check_stripe_connect
    if current_user.stripe_uid == nil
      render json: {message: false}
    else
      render json: {message: true}
    end
  end

  def check_if_item_approved
    @item = Item.find params[:id]
    unless @item.approved == false || :only_admins?
      flash[:alert] = "This Item need to be approved first by Admin"
      redirect_to home_path
    end
    # if @item.approved == false
    # end
  end

  private
    def check_item_shared
      @item = Item.find params[:id]
      unless @item.shared? || current_user == @item.user || current_user.following?(@item.user)
        redirect_to '/home'
      end
    end

  def item_params
    params.require(:item).permit(:name, :price, :qty, :desc, :all_tags, :sharing, :price_status, :status, :weekly_price, :monthly_price, :postal_code, :damage_deposit, :approved, :original_price)
  end
end
# item_attachments_attributes: [:id, :item_id, :image]
