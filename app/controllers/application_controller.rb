class ApplicationController < ActionController::Base
  before_filter :disable_nav, only: [:landing, :landing_main]
  before_filter :capture_referal
  # before_filter :set_online
  include SessionsHelper
  include PublicActivity::StoreController
  protect_from_forgery with: :exception
  rescue_from ActiveRecord::RecordNotFound, with: :render_404
  rescue_from ActiveSupport::MessageVerifier::InvalidSignature, with: :render_error

  def current_user
    @current_user ||= User.find_by_id session[:user_id]
  end
  helper_method :current_user

  def user_signed_in?
  current_user.present?
  end
  helper_method :user_signed_in?

  def reservation_count?
    Reservation.where('user_id = ? AND status = ?', current_user.id, 'paid').count
  end
  helper_method :reservation_count?

  def complete_reservations
    @reservations = Reservation.where('item_owner_id = ? AND status = ? AND checkedin_owner = ? AND checkedout_owner = ? AND checkedin_requester = ? AND checkedout_requester = ?', current_user.id, 'paid', 'yes', 'yes', 'yes', 'yes')
  end

  def user_credit
    Transaction.where(reservation_id: complete_reservations)
    # Transaction.group(:reservation_id).sum(:payout)
  end
  helper_method :user_credit


  # def sign_out?
  #   session[:user_id] = nil
  #   self.current_user = nil
  # end
  # helper_method :sign_out?

  def is_provider?
    # self.provider ||= 'facebook'
    current_user.provider == 'facebook'
  end
  helper_method :is_provider?


  def check_code?
    current_user.code_active == "active"
  end
  helper_method :check_code?

  def code_require!
    redirect_to welcome_path, alert: "Please type the correct code" unless check_code? # For lnading page
  end

  def authenticate_user!
    redirect_to root_path, alert: "Please sign in" unless user_signed_in?
    # redirect_to new_session_path, alert: "Please sign in" unless user_signed_in?
  end

  def only_admins?
    current_user.email == 'ali.alsaihaty@gmail.com' || current_user.email == 'alsaihaty.ali@gmail.com' || current_user.email == 'avinashbooluck@gmail.com' || current_user.email == 'avinashbooluck@yahoo.com'|| current_user.email == 'ramyead@gmail.com'
  end
  helper_method :only_admins?

  def only_admins!
    redirect_to home_path unless only_admins?
  end

  def has_beta?
    current_user.code_active == 'active'
  end
  helper_method :has_beta?

  def has_beta!
    redirect_to home_path if has_beta?
  end


  def disable_nav
    @disable_nav = true
  end


  def following
    @title = "Following"
    @user  = User.find(params[:id])
    @users = @user.following(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user  = User.find(params[:id])
    @users = @user.followers(page: params[:page])
    render 'show_follow'
  end


  def render_404
    respond_to do |format|
      format.html do
        render file: 'public/404.html', status: :not_found, layout: false
      end
      format.json do
        render status: 404, json: {
          message: "Not found."
        }
      end
    end
  end

  private

  def capture_referal
    session[:referral] = params[:referral] if params[:referral]
  end

  # def set_online
  #   if !!current_user
  #     $redis.set(current_user.id, nil, ex: 10*60)
  #   end
  # end

end
