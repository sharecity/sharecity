class UsersController < ApplicationController
  before_filter :disable_nav, only: [:new]
  before_action :authenticate_user!, :only => [:show]

  def new
    @user = User.new
    @user.referral_code = session[:referral]
    session[:user_id] = nil
  end

  def show
    @user = User.find params[:id]
    @conversation = Conversation.new
    @conversation.messages.new
  end

  def create_email
    @user = User.new user_params
    if session[:referral]
      @user.referral_code = session[:referral]
    end
    if @user.referral_code == "linksharing"
        if @user.save
          UserMailer.welcomeLinksharing(@user).deliver_now
          session[:referral] = nil
          flash[:success] = "Thanks for signing up with us"
          session[:user_id] = @user.id
          redirect_to welcome_path
        else
          flash[:alert] = "See errors below"
          render :new
        end
      else
        if @user.save
          UserMailer.welcome(@user).deliver_now
          session[:referral] = nil
          flash[:success] = "Thanks for signing up with us"
          session[:user_id] = @user.id
          redirect_to welcome_path
        else
          flash[:alert] = "See errors below"
          render :new
        end
      # end
      # redirect_to new_session_path
      # flash[:success] = "Please confirm your email address to continue"
      # redirect_to confirm_path
    # else
    #   flash[:alert] = "See errors below"
    #   render :new
    end
  end

  def edit
    @user = current_user
  end

  def update
    @user = current_user
    if params[:editAboutme]
      if @user.update(:about_me => params[:editAboutme])
        result = {message: "success", content: params[:editAboutme]}
        render json: result
        # redirect_to user_path(@user), notice: "About me has been updated"
      else
        flash[:alert] = "See errors below"
        render :edit
      end
    else
      if @user.update user_params
        redirect_to edit_users_path, notice: "Profile updated"
      else
        flash[:alert] = "See errors below"
        render :edit
      end
    end
  end

  def update_email
    @user = current_user
    if @user.authenticate(params[:user][:current_password])
      if @user.update user_params
        redirect_to edit_users_path, notice: "Profile updated"
      else
        flash[:alert] = "See errors below"
        render :edit
      end
    else
      flash[:alert] = "Wrong password"
      render :edit
    end
  end

  def code_update
    @user = current_user
    if params[:user][:code] == "aabb"
      if @user.update user_params
        @user.update(:code_active => 'active')
        redirect_to code_confirm_path, notice: "Valid Code. Start now using the platform. Thanks"
      else
        flash[:alert] = "See errors below"
        redirect_to code_confirm_path
      end
    else
      flash[:alert] = "Wrong code, please try again"
      redirect_to welcome_path
    end
  end

  def confirm_email
      @user = User.find_by_confirm_token params[:id]
      if @user
        @user.email_activate
        flash[:success] = "Welcome to the ShareCity! Your email has been confirmed.
        Please sign in to continue."
        redirect_to new_session_path
      else
        flash[:error] = "Sorry. User does not exist"
        redirect_to root_path
      end
  end

  def message
  end


  def update_credit_card
    @billing_address = Billing.find_by(user_id: current_user.id) || Billing.new
    token = params[:stripeToken]
    user = Stripe::Customer.retrieve(current_user.customer_id)
    old_card = user.sources.data[0]
    begin
      if old_card
        user.sources.retrieve(old_card.id).delete
      end
      user.sources.create(source: token)
      puts "success"
      @billing_address.update(
        address: params[:checkout]['billingAddress'],
        city: params[:checkout]['billingCity'],
        postalCode: params[:checkout]['billingPostalCode'],
        province_id: params[:checkout]['billingProvinceId'],
        country: '',
        user_id: current_user.id)
      flash[:card_update] = "Your billing information has been updated"
      redirect_to '/home'
    rescue Stripe::CardError => e
      BillingMailer.payment_failed(user).deliver
     redirect_to '/mygarden/billing-details'
    rescue Stripe::InvalidRequestError => e
      BillingMailer.payment_failed(user).deliver
      redirect_to '/home'
    end
  end

  private

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation, :avatar, :location, :postal_code, :code, :code_active, :referral_code, :about_me)
  end

end
