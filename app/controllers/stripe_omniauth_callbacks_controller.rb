class StripeOmniauthCallbacksController < ApplicationController

def stripe_connect
    @user = current_user
    if @user.update_attributes({
      stripe_provider: request.env["omniauth.auth"].provider,
      stripe_uid: request.env["omniauth.auth"].uid,
      access_code: request.env["omniauth.auth"].credentials.token,
      publishable_key: request.env["omniauth.auth"].info.stripe_publishable_key
    })
      # anything else you need to do in response..
      # sign_in_and_redirect @user, :event => :authentication
      redirect_to home_path
      set_flash_message(:notice, :success, :kind => "Stripe") if is_navigational_format?
    else
      session["stripe_omniauth.stripe_connect_data"] = request.env["omniauth.auth"]
      redirect_to home_path
    end
  end

end
