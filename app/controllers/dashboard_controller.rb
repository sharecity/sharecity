class DashboardController < ApplicationController
  def index
    @items = Item.page(params[:page]).per(28).order('created_at DESC')
    @conversations = Conversation.received(current_user)
  end
end
