class InvitesController < ApplicationController
  def new
    @invite = Invite.new
  end
  def create
    @invite      = Invite.new invite_params
    @invite.user = current_user
    inviter      = current_user
    if @invite.save
      invitee = @invite.email
      UserMailer.invite(current_user, invitee).deliver_now
      redirect_to home_path, notice: 'Thank you for your invitation'
    else
      flash.now[:error] = 'Cannot invite.'
      render :new
    end
  end
  private

  def invite_params
    params.require(:invite).permit(:email, :user_id)
  end
end
