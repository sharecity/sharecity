class RequestedItemsController < ApplicationController
  def new
    @requested_item = RequestedItem.new
  end
  def create
    @requested_item      = RequestedItem.new requested_params
    @requested_item.user = current_user
    if @requested_item.save
      redirect_to requested_items_path, notice: "The request has been posted!"
    else
      flash[:alert] = "An error occurred while creating this"
      render :new
    end
  end
  def index
    @requested_item = RequestedItem.new
    @requested_items = RequestedItem.all
  end
  def edit
    @requested_item = RequestedItem.find params[:id]
  end
  def update
    @requested_item = RequestedItem.find params[:id]
    if @requested_item.update requested_params
      redirect_to requested_items_path, notice: "Request Successfully updated!"
    else
      flash[:alert] = "See error(s) below..."
      render :edit
    end
  end
  def destroy
    @requested_item = RequestedItem.find params[:id]
    @requested_item.destroy
    redirect_to requested_items_path, notice: "Request Successfully deleted!"
  end
  private
  def requested_params
    params.require(:requested_item).permit(:name, :location, :user_id, :from_date, :to_date, :description)
  end
end
