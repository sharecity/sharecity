class TagsController < ApplicationController

  def index
    #  @tags = Tag.all
    # @tags_hash = []
    # @tags.each do |b|
    #   @tags_hash << b.name
    # end
    # render :json => @tags_hash
    # @tags = Tag.find(:all,:conditions => ['given_name LIKE ?', "#{params[:term]}%"])

    @tags = Tag.all.map(&:name).compact.reject(&:blank?)
    respond_to do |format|
      format.html
      format.json { render :json => @tags.to_json }
    end

    # render json: @tags.map{ |tag| {:name => tag.name} }

#     respond_to do |format|
#     format.html # index.html.erb
# # Here is where you can specify how to handle the request for "/people.json"
#     format.json { render :json => @tags.to_json }
    # end
  end

  def tagged_items
    @tag = Tag.find_by(name: params[:tagname])
    @tagged_items = @tag.items.where(approved: true)
  end

  def show
    @tag = Tag.find params[:id]
  end

  private

  def tag_params
    params.require(:tag).permit(:name)
  end

end
