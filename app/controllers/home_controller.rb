class HomeController < ApplicationController
  skip_before_filter :verify_authenticity_token
  before_filter :authenticate_user!, only: :index
  before_filter :code_require!, only: :index

  def index
    @items = current_user.items.order(created_at: :desc)
    @pending = current_user.reservations.pending.order(created_at: :desc)
    @allItems = Item.all
    @feedItems = Item.all.where(approved: true).order(created_at: :desc)
  end

  def pub
    @conversation = Conversation.last
    @message = @conversation.messages.build
  end

  def msg
    MessageBus.publish '/private/'+ params[:rid], {msg: params[:msg], rid: params[:rid], from: current_user.name}
    render json: {status: 'yay'}
  end

  def authorize
    respond_to do |format|
      format.html
    end
  end
end
