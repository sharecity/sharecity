class BillingsController < ApplicationController
  def new
    @billing = Billing.new
  end

  def create
   #  @billing = Billing.new(address: params[:addressJ], city: params[:cit], country: params[:countryJ], postal_code: params[:postalJ], user_id: params[:usrId])
   address        = params[:address]
   city           = params[:city]
   country        = params[:country]
   postal         = params[:postal]
   province_state = params[:pro_st]
   token          = params[:stripeToken]
   currency       = params[:currency]
    begin
      newCustomerStripe = Stripe::Customer.create(
      :source => token,
      :email => current_user.email,
      :description => 'ShareCity - New Customer Id for ' + current_user.first_name + " " + current_user.last_name
      )
      User.update(current_user.id, :customer_id => newCustomerStripe.id)
      current_user.billing = Billing.create(address: address, city: city, country: country, postal_code: postal, user_id: current_user.id, address_state: province_state)
      if current_user.billing.save
        render json: {status: true, message: "Your billing address has been created!"}
        # redirect_to home_path, notice:"Your billing address has been created!"
      else
        redirect_to home_path, alert:"Something went wrong--please refresh the page and try again."
      end
    rescue Stripe::RateLimitError => e
      # Too many requests made to the API too quickly
      render json: {status: false, error: e.message}
    rescue Stripe::InvalidRequestError => e
      # Invalid parameters were supplied to Stripe's API
      render json: {status: false, error: e.message}
    rescue Stripe::AuthenticationError => e
      # Authentication with Stripe's API failed
      render json: {status: false, error: e.message}
    rescue Stripe::APIConnectionError => e
      # Network communication with Stripe failed
      render json: {status: false, error: e.message}
    rescue Stripe::StripeError => e
      # Display a very generic error to the user, and maybe send
      # yourself an email
      render json: {status: false, error: e.message}
    rescue => e
      render json: {status: false, error: e.message}
    rescue Stripe::CardError => e
      render json: {status: false, error: e.message}
      # redirect_to home_path, alert: e.message
    end
  end

  def charge
    reservation = params[:reservation_id].to_i
    amount      = Reservation.where(id: reservation)[0].total.to_i
    # @billing = Billing.find params[:id]
    begin
      charge = Stripe::Charge.create(
      :customer    => current_user.customer_id,
      :amount      => amount * 100,
      :description => 'Rails Stripe customer',
      :currency    => 'cad'
      )
      Reservation.update(reservation, :status => "paid")
    rescue Stripe::RateLimitError => e
      render json: {status: false, error: e.message}
    rescue Stripe::InvalidRequestError => e
      render json: {status: false, error: e.message}
    rescue Stripe::AuthenticationError => e
      render json: {status: false, error: e.message}
    rescue Stripe::APIConnectionError => e
      render json: {status: false, error: e.message}
    rescue Stripe::StripeError => e
      render json: {status: false, error: e.message}
    rescue Stripe::CardError => e
      render json: {status: false, error: e.message}
    rescue => e
      render json: {status: false, error: e.message}
    end
  end

  def update
    @billing = Billing.find params[:id]
    if @billing.update billing_params
      redirect_to billing_path(@billing), notice: "Billing info successfully updated!"
    else
      flash[:alert] = "See error(s) below..."
      render :edit
    end
  end

  def edit
     @billing = Billing.find params[:id]
  end

  def show
    @billing = Billing.find params[:id]
  end

  def index
    @billings = Billing.all
    render json: @billings.as_json(
    only: [:id],
    include: {
      user: { only: [:id]} } )
  end

  private
    def billing_params
	     params.require(:billing).permit(:address, :city, :postal_code, :country, :user_id, :stripeToken)
    end
end
