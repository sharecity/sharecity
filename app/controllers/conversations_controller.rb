class ConversationsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :code_require!
  before_action :find_conversations
  def index
    @users = User.all
    @conversations = current_user.all_conversations
    @sent = Conversation.sent(current_user)
  end

  def create
    @conversation = Conversation.new(
      recipient_id: params[:rid].to_i,
      sender_id: params[:sid].to_i
    )
    if @conversation.save
      @message = @conversation.messages.create(
        body: params[:body],
        user_id: params[:sid].to_i
      )

      data = { body: params[:body], rid: params[:rid], sid: current_user.id, from: current_user.full_name.capitalize, cid: @conversation.id, avatar: current_user.img }
      MessageBus.publish "/conversations/from-#{current_user.id}", data
      MessageBus.publish "/conversations/to-#{params[:rid]}", data

      render json: { cid: @conversation.id }
    else
      render json: { error: 'error' }
    end
  end

  def read
    @conversation = Conversation.find(params[:cid].to_i)
    @conversation.messages.recipient(current_user).update_all(read: true)
    render json: { unread: current_user.unread_count }
  end

  def destroy
    @conversation = Conversation.find(params[:id])
    @conversation.messages.destroy
    @conversation.destroy
    redirect_to conversations_path
  end

  private

  def conversation_params
    params.require(:conversation).permit(:sender_id, :recipient_id, :reservation_id,
                messages_attributes: [:body, :user_id, :reservation_id, :read, :conversation_id] )
  end

  def find_conversations
    @conversations = Conversation.where('recipient_id = ? OR sender_id = ?', current_user, current_user)
  end
end
