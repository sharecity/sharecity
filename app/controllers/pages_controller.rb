class PagesController < ApplicationController
  # before_filter :code_require!, :only => :landing
  layout "landing-layout", :only => [:landing, :landing_main]
  # layout "confirm-layout", :only => :confirm
  before_action :authenticate_user!, :only => [:welcome, :confirm, :approval]
  before_action :redirect_home, :only => [:landing_main]
  before_action :only_admins!, :only => [:approval]
  before_action :has_beta!, :only => [:welcome]

  def about
  end

  def faq
  end

  def blog
  end

  def terms
  end

  def landing
  end

  def landing_main
  end

  def welcome
  end

  def code_confirm
  end

  def code_wrong
  end

  def confirm
    render layout: "confirm-layout"
  end

  def careers
  end

  def noItem
  end

  def help
  end

  def test
  end

  def approval
    @items = Item.where(approved: false).order('created_at DESC')
  end

  private

  def redirect_home
    if user_signed_in?
      redirect_to home_path
    end
  end

  def method_name

  end

end
