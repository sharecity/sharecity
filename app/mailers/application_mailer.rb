class ApplicationMailer < ActionMailer::Base
  default from: "info@sharecity.net"
  layout 'mailer'
end
