class UserMailer < ActionMailer::Base
   default from: "info@sharecity.net"
   before_filter :add_inline_attachments!, except: [:itemRequest, :itemRejected] # Add image header for all actions
  #  before_filter :item_req_attachments!, only: [:itemRequest, :itemRejected]

   def registration_confirmation(user)
      @user = user
      # attachments.inline['logo.png'] = File.read('path/to/logo.png')
      mail to: "#{user.name} <#{user.email}>", subject: "Registration Confirmation"
   end

   def password_reset(user)
    @user = user
    mail to: @user.email, subject: "Password reset"
  end

  def welcome(user)
    @user = user
    mail to: @user.email, subject: "Welcome to ShareCity"
  end

  def welcomeLinksharing(user)
    @user = user
    mail to: @user.email, subject: "Welcome to ShareCity"
  end

  def requestPending(owner, requester, reservation_id)
    @owner 	        = owner
    @requester      = requester
    status          = "requested"
    @reservation    = Reservation.where(id: reservation_id, status: status, user_id: @requester.id)
    # @reservations = Reservation.where(status: status, user_id: @requester.id).last
    @itemRequested  = @reservation[0].item
    @itemAttachment = ItemAttachment.where(item_id: @itemRequested.id).first
    mail to: @owner.email, subject: "Item Request - ShareCity"
  end

  # def item_confirm(owner, requester)
  #   @owner 	       = owner
  #   @requester     = requester
  # end

  def itemApproval(owner, requester, reservation_id)
    @owner 	        = owner
    @requester      = requester
    status             = "approved-owner"
    @reservation    = Reservation.where(id: reservation_id, status: status, user_id: @requester.id)
    mail to: @requester.email, subject: "Item request approval - ShareCity"
  end

  def itemRejected(owner, requester)
    @owner 	        = owner
    @requester      = requester
    status             = "rejected-owner"
    @reservation    = Reservation.where(status: status, user_id: @requester.id).first
    mail to: @requester.email, subject: "Item request denied  - ShareCity"
  end

  def bookingConfirmed(owner, reservation_id, transaction)
    @owner 	        = owner
    @transaction    = transaction
    @reservation    = Reservation.find(reservation_id)
    mail to: @owner.email, subject: "New Item booking confirmed - ShareCity"
  end

  def invite(inviter, invitee)
    @inviter = inviter
    @invitee = invitee
    mail to: @invitee, subject: "ShareCity - Invitation"
  end

  def following_you(followed, following)
    @followed  = followed
    @following = following
    mail to: @followed.email, subject: "ShareCity - Following"
  end

  def new_message(followed, following)
    @followed  = followed
    @following = following
    mail to: @followed.email, subject: "ShareCity - New message"
  end

  def new_claim(claimer, claim)
    @claimer = claimer
    @claim   = claim
    mail to: "avi.ramyead@sharecity.net", subject: "ShareCity - New claim"
  end

  def itemPreApproval

    mail to: "avi.ramyead@sharecity.net", subject: "ShareCity - New claim"
  end

  private
   def add_inline_attachments!
   attachments.inline['logo.png'] = File.read("#{Rails.root}/app/assets/images/logo.png")
   attachments.inline['link.png'] = File.read("#{Rails.root}/app/assets/images/link.png")
   attachments.inline['facebook1.png'] = File.read("#{Rails.root}/app/assets/images/facebook1.png")
   attachments.inline['twitter1.png'] = File.read("#{Rails.root}/app/assets/images/twitter1.png")
   attachments.inline['linkedin1.png'] = File.read("#{Rails.root}/app/assets/images/linkedin1.png")
   attachments.inline['instagram1.png'] = File.read("#{Rails.root}/app/assets/images/instagram1.png")
   attachments.inline['pinterest.png'] = File.read("#{Rails.root}/app/assets/images/pinterest.png")
   attachments.inline['youtube.png'] = File.read("#{Rails.root}/app/assets/images/youtube.png")
  end
  def item_req_attachments!
   attachments.inline['logo.png'] = File.read("#{Rails.root}/app/assets/images/logo.png")
  end

end
