ActiveAdmin.register User do

  index do
    column :id
    column :first_name
    column :last_name
    column :email
    column :postal_code
    column :provider
    column :created_at
    actions
  end


end
