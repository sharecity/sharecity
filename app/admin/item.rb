ActiveAdmin.register Item do

  # permit_params

    index do
      column :id
      column :name
      column :price
      column :price_status
      column :postal_code
      column :user_id
      column :created_at
      actions
    end

end
