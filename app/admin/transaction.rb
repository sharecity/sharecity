ActiveAdmin.register Transaction do

  permit_params :user_id, :confirmation_id, :amount, :fees, :payout, :reservation_id, :item_id

    index do
      column :id
      column :confirmation_id
      column :user_id
      column :amount
      column :fees
      column :payout
      column :transfered
      column :item_id
      column :reservation_id
      actions
    end


end
