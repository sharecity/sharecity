ActiveAdmin.register Reservation do

  permit_params :start_date, :end_date, :price, :total, :user_id, :created_at, :item_id, :item_owner_id, :status, :checkedin_owner, :checkedout_owner, :checkedout_requester, :checkedin_requester

    index do
      column :id
      column :start_date
      column :end_date
      column :price
      column :total
      column :status
      column :user_id
      column :item_id
      column :item_owner_id
      column :checkedin_owner
      column :checkedout_owner
      column :checkedout_requester
      column :checkedin_requester
      actions
    end

    form do |f|
    f.inputs do
      f.input :start_date
      f.input :end_date
      f.input :price
      f.input :total
      f.input :item_id
      f.input :item_owner_id
      f.input :checkedin_owner
      f.input :checkedout_owner
      f.input :checkedout_requester
      f.input :checkedin_requester
    end
    f.actions
  end


end
