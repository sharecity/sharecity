class Conversation < ActiveRecord::Base

  belongs_to :sender, :foreign_key => :sender_id, :class_name => 'User'
  belongs_to :recipient, :foreign_key => :recipient_id, :class_name => 'User'

  has_many :messages, dependent: :destroy

  belongs_to :reservation
  accepts_nested_attributes_for :messages

  # validates_uniqueness_of :sender_id, :scope => :recipient_id

  scope :between, -> (sender_id,recipient_id) do
    where("(conversations.sender_id = ? AND conversations.recipient_id =?) OR (conversations.sender_id = ? AND conversations.recipient_id =?)", sender_id,recipient_id, recipient_id, sender_id)
  end

  scope :sent, -> (user) { where('sender_id = ?', user) }
  scope :received, -> (user) { where('recipient_id = ?', user) }

  def message_time
    created_at.strftime("%l:%M %p - %B #{created_at.day.ordinalize}")
  end

  def sender?(user)
    sender == user
  end

  def recipient?(user)
    recipient == user
  end

  def partner(user)
    user == recipient ? sender : recipient
  end

  def unread_msgs?(user)
    messages.recipient(user).unread?.exists?
  end
end
