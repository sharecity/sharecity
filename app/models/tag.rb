class Tag < ActiveRecord::Base

  has_many :taggings
  has_many :items, through: :taggings


 # before_save :capitalize_all_tags
 #
 #  def capitalize_all_tags
 #    # self.title.capitalize!
 #    self.all_tag = all_tag.capitalize
 #  end

 scope :starts_with, -> (name) { where("name like ?", "#{name}%")}


 def self.search(term)
    Tag.where('name LIKE ?', "%de%")
  end

  def self.counts
    self.select("name, count(taggings.tag_id) as count").joins(:taggings).group("taggings.tag_id", "tags.name")
  end

end
