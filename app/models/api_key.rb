class ApiKey < ActiveRecord::Base

  before_create :generate_access_token

  def self.generate_access_token

    hmac_secret = 'my$ecretK3y'

    payload = {:data => 'test'}
    access_token = JWT.encode payload, hmac_secret, 'HS256'
  end

end
