class User < ActiveRecord::Base
  after_create :subscribe
  attr_accessor :remember_token, :activation_token, :reset_token
  before_create :confirmation_token, :only => :create_email?
  before_create :generate_authentication_token
  has_secure_password(validations: false)
  ratyrate_rater
  mount_uploader :avatar, AvatarUploader
  acts_as_mappable
  has_one :billing
  has_many :items
  has_many :requested_items
  has_many :reservations
  has_many :reviews
  has_many :conversations, foreign_key: :sender_id, dependent: :destroy
  has_many :received_conversations, class_name: 'Conversation',
                                    foreign_key: :recipient_id,
                                    dependent: :destroy
  has_many :active_relationships, class_name:  "Relationship",
                                  foreign_key: "follower_id",
                                  dependent:   :destroy
  has_many :passive_relationships, class_name:  "Relationship",
                                   foreign_key: "followed_id",
                                   dependent:   :destroy
  has_many :following, through: :active_relationships, source: :followed
  has_many :followers, through: :passive_relationships, source: :follower

  has_one :profile
  # has_one :item_attachment, dependent: :destroy

 validates :first_name, presence:   {message: "must be present"},
                         length:     {minimum: 2}

 validates :last_name, presence:   {message: "must be present"}

 # validates :postal_code, {presence: true}


 validates :email, presence: {message: "must be present"}, uniqueness: true,
            format: /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i

  def self.from_omniauth(auth)
  where(email: auth.info.email).first_or_create do |user|
    user.provider         = auth.provider
    user.uid              = auth.uid
    user.first_name       = auth.info.first_name
    user.last_name        = auth.info.last_name
    user.email            = auth.info.email
    user.oauth_token      = auth.credentials.token
    user.oauth_expires_at = Time.at(auth.credentials.expires_at)
    user.image            = auth.info.image
    user.gender           = auth.extra.raw_info.gender
    user.location         = auth.info.location
    # user.location         = auth.extra.raw_info.locale
    # user.url              = auth_hash['info']['urls'][user.provider.capitalize]
    user.save(validate: false)
    # UserMailer.welcome(user).deliver_now
  end
  # byebug
 end


 validates_confirmation_of :password, on: :create, :unless => :from_omniauth?
 validates_presence_of :password_digest, :unless => :from_omniauth?

 # postal_code is a field in our database in this case. We expect the user to
 # provide the postal_code of the item
 geocoded_by :postal_code               # can also be an IP address
 # Ideally you will make the geocoding happen in the background
 # because it connects to a third party service. So you shouldn't have the
 # web request waitng for that.
 after_validation :geocode

 # def online?
 #  $redis.exists( self.id )
 # end

 def increment_login_count
    increment! :sign_in_count
  end

 def from_omniauth?
   provider && uid
 end


 # def friendslist
 #   facebook {|fb| fb.get_connection("me", "friends")}.each do |hash|
 #     self.friends.find_or_create_by(:name => hash['name'], :uid => hash['id'])
 #     end
 # end

 def facebook
   @facebook ||= Koala::Facebook::API.new(oauth_token)

  #  @facebook.get_object('me') # I'm a page
  #  @facebook.get_connection('me', 'feed') # the page's wall
  #  @facebook.put_wall_post('post on page wall') # post as page, requires new publish_pages permission
  #  @facebook.put_connections(page_id, 'feed', :message => message, :picture => picture_url, :link => link_url)
 end

  def confirmation_token
    if self.confirm_token.blank?
      self.confirm_token = SecureRandom.urlsafe_base64.to_s
    end
  end

  def email_activate
    self.email_confirmed = true
    self.confirm_token = nil
    save!(:validate => false)
  end

  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest,  User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  # Returns the hash digest of the given string.
  def self.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # Returns a random token.
  def self.new_token
    SecureRandom.urlsafe_base64
  end

  # Returns true if a password reset has expired.
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end

  # Follows a user.
  def follow(other_user)
    active_relationships.create(followed_id: other_user.id)
  end

  # Unfollows a user.
  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end

  # Returns true if the current user is following the other user.
  def following?(other_user)
    following.include?(other_user)
  end

  def subscribe
    MailingListSignupJob.perform_later(self)
  end

  def full_name
    "#{first_name} #{last_name}".strip
  end

  def fullname_abbrev
    "#{first_name.capitalize} #{last_name[0].capitalize}"
  end

  def img
    case provider
    when 'facebook'
      "//graph.facebook.com/#{uid}/picture?type=square"
    when 'google_oauth2'
      image
    else
      avatar.url
    end
  end

  def all_conversations
    all = conversations + received_conversations
    all.sort { |c1, c2| c2.updated_at <=> c1.updated_at }
  end

  def unread_count
    all_conversations.reduce(0) do |sum, convo|
      sum + convo.messages.recipient(self).unread?.count
    end
  end

  def unread?
    unread_count > 0
  end

  private

  def generate_authentication_token
    loop do
      self.authentication_token = SecureRandom.base64(64)
      break unless User.find_by(authentication_token: authentication_token)
    end
  end

end
