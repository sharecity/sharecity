class Transaction < ActiveRecord::Base
  belongs_to :item
  belongs_to :user
  belongs_to :reservation

  def refundable?
    if deposit_charge
      charge = Stripe::Charge.retrieve(deposit_charge)
      not_refunded = charge.refunded == false
    else
      not_refunded = false
    end
    not_refunded && damage_deposit > 0
  end
  
end
