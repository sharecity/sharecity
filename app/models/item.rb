class Item < ActiveRecord::Base

  include PublicActivity::Model
  tracked only: [:create], owner: Proc.new{ |controller, model| controller.current_user }

  ratyrate_rateable 'visual_effects'
  before_save :capitalize_name

  has_many :item_attachments, dependent: :delete_all
  has_many :reservations, dependent: :nullify
  has_many :transactions, dependent: :nullify
  has_many :unavailable_dates, dependent: :delete_all
  has_many :reviews, dependent: :delete_all
  has_many :taggings, dependent: :delete_all
  has_many :tags, through: :taggings

  # accepts_nested_attributes_for :reservations
  # accepts_nested_attributes_for :item_attachments, reject_if: :all_blank
  accepts_nested_attributes_for :unavailable_dates
  accepts_nested_attributes_for :item_attachments, :reject_if => proc {|attributes| attributes['filename'].blank? && attributes['filename_cache'].blank?}

  belongs_to :user
  # delegate :name, to: :tag, prefix: true

  validates :name, presence:true
  validates :desc, presence:true
  validates :postal_code, presence:true
  validates :all_tags, presence:true
  validates :price, presence:true, unless: :price_status?
  # validates :price, numericality: { greater_than_or_equal_to: 3 }
  validates :damage_deposit, numericality: true

  # postal_code is a field in our database in this case. We expect the user to
  # provide the postal_code of the item
  geocoded_by :postal_code               # can also be an IP address
  # Ideally you will make the geocoding happen in the background
  # because it connects to a third party service. So you shouldn't have the
  # web request waitng for that.
  after_validation :geocode

  # reverse_geocoded_by :latitude, :longitude
  # after_validation :reverse_geocode  # auto-fetch address

  scope :not_shared, -> { where('sharing = ?', '1') }
  scope :shared, -> { where('sharing =?', '0') }

  def all_tags=(names)
    self.tags = names.split(",").map do |name|
        Tag.where(name: name.strip.capitalize).first_or_create!
    end
  end

  def all_tags
    self.tags.map(&:name).join(", ")
  end

  def self.tagged_with(name)
    Tag.find_by_name!(name).items
  end

  def shared?
    sharing == '0'
  end

  def free?
    price_status == '1'
  end

  def capitalize_name
    self.name = self.name.split.collect(&:capitalize).join(' ') if self.name && !self.name.blank?
  end

  # def self.tag_counts
  #   Tag.select("tags.*, count(taggings.tag_id) as count").joins(:taggings).group("taggings.tag_id")
  # end
#   def self.search(name)
#   Tag.where('name LIKE :name', name: "%name%")
# end
  def self.search(search)
    search_length = search.split.length
    key_words = search.to_s.split(", ")
    cc = key_words.map { |s| "'#{s.slice(0,1).capitalize + s.slice(1..-1)}'" }.join(', ')
    Item.find_by_sql("SELECT items.*, COUNT(*) FROM items INNER JOIN taggings ON taggings.item_id = items.id INNER JOIN tags ON tags.id = taggings.tag_id WHERE tags.name IN (#{cc}) GROUP BY items.id having COUNT(*)=#{search_length}")
  end

end
