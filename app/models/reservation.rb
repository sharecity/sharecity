class Reservation < ActiveRecord::Base
  # include PublicActivity::Model
  # tracked only: [:updateWithPay], owner: Proc.new{ |controller, model| controller.current_user }

  before_save :default_values

  has_many :messages
  has_one :conversation

  belongs_to :user
  belongs_to :item


  STATUSES = { 'approved' => 'Reservation is placed',
               'requested' => 'Reservation has been requested',
               'rejected' => 'Reservation has been rejected'}
  scope :pending, -> { where('status = ?', 'requested') }
  scope :confirming, -> { where('status = ?', 'confirmed') }
  scope :paid, -> { where('status = ?', 'paid') }
  scope :accepted, -> { where('status = ?', 'approved-owner') }
  scope :rejected, -> { where('status = ?', 'rejected-owner') }
  scope :readyForCheckInOwner, -> { where('status = ? AND checkedin_owner = ?', 'paid', 'no') }
  scope :readyForCheckInRequester, -> { where('status = ? AND checkedin_requester = ?', 'paid', 'no') }
  scope :readyForCheckOutOwner, -> { where('status = ? AND checkedout_owner = ? AND checkedin_owner = ?', 'paid', 'no', 'yes') }
  scope :readyForCheckOutRequester, -> { where('status = ? AND checkedout_requester = ? AND checkedin_requester = ?', 'paid', 'no', 'yes') }
  def self.valid_statuses
    STATUSES.keys
  end

  def current_status
    self.status
  end

  # validates :status, inclusion: { in: valid_statuses }, presence: true

  def extended_status
    STATUSES[status]
  end

  def default_values
    self.status ||= 'requested'
  end

  def approved_status
    self.status ||= 'approved'
  end

  def rejected_status
    self.status ||= 'rejected'
  end

  def all_checked_in?
    checkedin_requester == 'yes' && checkedin_owner == 'yes'
  end

  def all_checked_out?
    checkedout_requester == 'yes' && checkedout_owner == 'yes'
  end

  def get_transaction
    Transaction.find_by(reservation_id: id)
  end
end
