class Message < ActiveRecord::Base
  belongs_to :conversation
  belongs_to :user
  # belongs_to :reservation
  # validates_presence_of :reservation_id
  # validates_presence_of :body, :conversation_id, :user_id

  scope :unread?, -> { where('read IS NULL') }
  scope :recipient, -> (user) { where('user_id != ?', user) }

  def message_time
    created_at.strftime("%l:%M %p")
  end
end
