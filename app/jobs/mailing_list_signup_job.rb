class MailingListSignupJob < ActiveJob::Base

  def perform(user)
    # byebug
    logger.info "signing up #{user.email}"
    subscribe(user)
  end

  def subscribe(user)
    mailchimp = Gibbon::Request.new(api_key: Rails.application.secrets.mailchimp_api_key)
    list_id_friends      = Rails.application.secrets.mailchimp_list_id_friends
    list_id_schools      = Rails.application.secrets.mailchimp_list_id_schools
    list_id_linksharing  = Rails.application.secrets.mailchimp_list_id_linksharing
    list_id_socialmedia  = Rails.application.secrets.mailchimp_list_id_socialmedia
    list_id_bloggers     = Rails.application.secrets.mailchimp_list_id_bloggers
    list_id_unknown      = Rails.application.secrets.mailchimp_list_id_unknown
    # byebug
    if user.referral_code == "friends"
      result = mailchimp.lists(list_id_friends).members.create(
        body: {
          email_address: user.email,
          status: 'subscribed',
          merge_fields: {FNAME: user.first_name, LNAME: user.last_name}
          # status: 'pending'
      })
      Rails.logger.info("Subscribed #{user.email} to MailChimp") if result
      # begin
      #   gibbon.campaigns.create(body: body)
      # rescue Gibbon::MailChimpError => e
      #   puts "Houston, we have a problem: #{e.message} - #{e.raw_body}"
      # end
    elsif user.referral_code == "schools"
      result = mailchimp.lists(list_id_schools).members.create(
        body: {
          email_address: user.email,
          status: 'subscribed',
          merge_fields: {FNAME: user.first_name, LNAME: user.last_name}
          # status: 'pending'
      })
      Rails.logger.info("Subscribed #{user.email} to MailChimp") if result
    elsif user.referral_code == "linksharing"
      result = mailchimp.lists(list_id_linksharing).members.create(
        body: {
          email_address: user.email,
          status: 'subscribed',
          merge_fields: {FNAME: user.first_name, LNAME: user.last_name}
          # status: 'pending'
      })
      Rails.logger.info("Subscribed #{user.email} to MailChimp") if result
    elsif user.referral_code == "socialmedia"
      result = mailchimp.lists(list_id_socialmedia).members.create(
        body: {
          email_address: user.email,
          status: 'subscribed',
          merge_fields: {FNAME: user.first_name, LNAME: user.last_name}
          # status: 'pending'
      })
      Rails.logger.info("Subscribed #{user.email} to MailChimp") if result
    elsif user.referral_code == "bloggers"
      result = mailchimp.lists(list_id_bloggers).members.create(
        body: {
          email_address: user.email,
          status: 'subscribed',
          merge_fields: {FNAME: user.first_name, LNAME: user.last_name}
          # status: 'pending'
      })
      Rails.logger.info("Subscribed #{user.email} to MailChimp") if result
    else
      # byebug
      result = mailchimp.lists(list_id_unknown).members.create(
        body: {
          email_address: user.email,
          status: 'subscribed',
          merge_fields: {FNAME: user.first_name, LNAME: user.last_name}
          # status: 'pending'
      })
      Rails.logger.info("Subscribed #{user.email} to MailChimp") if result
    end

  end

end
