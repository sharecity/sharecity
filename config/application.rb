# require 'rack/redis_throttle'
require File.expand_path('../boot', __FILE__)

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "sprockets/railtie"
require 'message_bus'
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)
Dotenv::Railtie.load

unless Rails.env.development? 
  if defined?(PhusionPassenger)
    PhusionPassenger.advertised_concurrency_level = 0
    PhusionPassenger.on_event(:starting_worker_process) do |forked|
      if forked
        #We're in smart spawning mode.
        MessageBus.after_fork
      else
        # We're in conservative spawning mode. We don't need to do anything.
      end
    end
  end
end
module ShareCity
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.middleware.delete Rack::Lock
    # config.middleware.use MessageBus::Rack::Middleware
    #config.middleware.use FayeRails::Middleware, mount: '/faye', timeout: 25
    config.assets.paths += Dir["#{Rails.root}/vendor/asset-libs/*"].sort_by{|dir| -dir.size}
    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = 'Pacific Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de
      config.i18n.available_locales = [:en, :es, :de]

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true

    # config.action_view.embed_authenticity_token_in_remote_forms = true

    # config.middleware.use Rack::RedisThrottle::Daily, max: 100000
    # config.assets.paths << "#{Rails.root}/app/public/videos"
    config.assets.paths << "#{Rails.root}/app/assets/videos"

  end
end
