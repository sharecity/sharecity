OmniAuth.config.logger = Rails.logger

Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook, '989408694457545', 'a46bc1da86d1a3e2e02437b93dfe341a',
  {
  scope: "email, public_profile, user_birthday, user_location, read_custom_friendlists, user_friends", :display => 'popup',
  info_fields: 'id, name, first_name, middle_name, last_name, age_range, link, gender, locale, timezone, updated_time, verified, email, birthday, location'
  }
  provider :google_oauth2, '111121626924-v1aunov2her3vqhcm66f1vlft3v0n23m.apps.googleusercontent.com', '49d5bwfvZHuNIpNucU4M64Mz',
  {
  :scope => "email, profile, plus.me",
  :prompt => "select_account",
  :image_size => {:width => 140, :height => 140},
  :skip_jwt => true
  }
  provider :stripe_connect, 'ca_9Nn8JK22XzC5a6mIjbvLepzqv9SnO9NO', 'sk_test_QyZk0Wf9uEoCyWtxfdzg4MPX',
  {
    :scope => 'read_write',
    :stripe_landing => 'register'
  }

end
if Rails.env.development?
 OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
end

# , display: 'popup'

# Added to config/initializers/omniauth.rb
OmniAuth.config.on_failure = Proc.new { |env|
  OmniAuth::FailureEndpoint.new(env).redirect_to_failure
}
