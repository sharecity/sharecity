Rails.application.routes.draw do

  get 'claims/index'

  get 'claims/new'

  # PRIVATE PUB TESTS
  get '/pub' => 'home#pub'
  post '/msg' => 'home#msg'

  get 'paypal_accounts/new'

  get 'paypal_accounts/edit'

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  match '/invites', to: 'invites#new', via: 'get'
  resources :invites, only: [:new, :create]

  resources :requested_items
  resources :billings
  # match '/billings/charge' => 'billings#charge', via: 'patch'
  put '/reservations/:id/updateWithPay' => 'reservations#updateWithPay'
  put '/reservations/:id/updateCheck' => 'reservations#updateCheck'
  put '/approveItem' => 'items#approveItem'

  get '/checkStripe' => 'items#check_stripe_connect'
  get '/charge' => 'billings#charge'
  get 'reviews/new'
  get 'reviews/show'
  get 'reviews/index'

  get 'dashboard/index'

  # post "emailapi/subscribe" => 'emailapi#subscribe'

  # get '/about' => 'pages#about'
  # get '/faq' => 'pages#faq'

  post '/rate' => 'rater#create', :as => 'rate'

  get '/conversations/:conversation_id/messages' => 'messages#show', as: :message
  post '/conversations/messages/new' => 'messages#new_message'
  post '/conversations/read' => 'conversations#read'

  post '/newclaim' => 'reservations#newclaim'

  resources :conversations, except: :show
  resources :activities
  # resources :profiles

  match "/contacts/:importer/callback" => "invites#new", :via => [:get]
  # google contacts callback
  match "/oauth2callback" => "main#contact", :via => [:get]
  # Yahoo contacts callback
  match "/callback" => "main#contact", :via => [:get]

  match "/send-invites" => "main#invite", :via => [:get]
  match "/import-emails" => 'invites#new', :via => [:get]

  match '/contacts', to: 'contacts#new', via: 'get'
  resources :contacts, only: [:new, :create]

  post '/items/share' => 'items#share'
  get '/items/item_preview/:id' => 'items#item_preview', as: :shareItem

  # get 'tags/index'
  resources :items do
    resources :reviews, only: [:create, :destroy]
  end
  resources :search_suggestions
  resources :password_resets,   only: [:new, :create, :edit, :update]
  resources :relationships,     only: [:create, :destroy]

  resources :item_attachments
  match '/item_attachments_delete' =>'item_attachments#destroy_img', :via => [:destroy]


  resources :items do
    resources :reservations, only: [:create, :update]
  end

  get '/preloadDates' => 'items#preloadDates'
  # get '/preload' => 'reservations#preload'

  get '/preload' => 'reservations#preload'
  get '/preview' => 'reservations#preview'
  resources :reservations, only: [:index]

  resources :users, only: [:new, :create, :show] do
    collection do
      get   :edit
      patch :update
    end
    member do
      get :confirm_email
      get :following, :followers
    end
  end

  match '/auth/stripe_connect' =>'stripe_omniauth_callbacks#passthru', :via => [:get, :post]
  match '//auth/stripe_connect/callback' =>'stripe_omniauth_callbacks#stripe_connect', :via => [:get, :post]

  post '/users/create_email/' => 'users#create_email'
  get '/users/create_email/edit' => 'users#edit'
  patch '/users/create_email/' => 'users#update_email'
  patch '/users/code_update/' => 'users#code_update'
  put '/about_me_edit/' => 'users#update'
  # get 	'/message'  => 'users#message'
  # patch '/users/create_email' => 'users#create_email', :as => 'users/create_email'

  match '/tags/index' => 'tags#index', :via => :get
  # match '/tags/:id' => 'tags#show', :via => :get
  get 'tags/:tagname' => 'tags#tagged_items', as: :tagged_items
  get 'tags/:tag', to: "tags#index", as: "tag"

  match 'sessions/create_email' => 'sessions#create_email', :via => :post

  # match 'sessions/create_email' => 'sessions#create_email', :via => :post

  resources :sessions, only: [:new, :create, :destroy]
  get 'auth/:provider/callback', to: 'sessions#create', via: [:get, :post]
  get 'auth/failure', to: 'sessions#failure', via: [:get, :post]
  # get 'auth/failure', to: redirect('/'), via: [:get, :post]
  get 'signout', to: 'sessions#destroy', as: 'signout', via: [:get, :post]

  root 'pages#landing_main'
  get '/home' => 'home#index', as: "home", via: [:get]




  #api
  # namespace :api do
  #   namespace :v1 do
  #     resources :users, only: [:index, :create, :show, :update, :destroy]
  #     resources :items, only: [:index, :create, :show, :update, :destroy]
  #     # resources :sessions, only: [:create]
  #     match '/sessions' => 'sessions#create', :via => :get
  #
  #     get '/login' => 'users#login'
  #   end
  # end

  PagesController.action_methods.each do |action|
    get "/#{action}", to: "pages##{action}"
  end

end
